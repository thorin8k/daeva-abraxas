var gulp = require('gulp');
var del = require('del');
var gulpIf = require('gulp-if');
var uglify = require('gulp-uglify');
var exec = require('child_process').exec;

var appName = "SeigyoPanel";
var appVersion = "0.1";

var temp = './out/temp';
var dist = './out/dist';

gulp.task('clean', function () {
    return del([
        temp + '/**/*',
        dist + '/**/*'
    ]);
});

/**
 * Minificacion de archivos js y mover a temporales
 */
gulp.task('compile', ['clean'], function () {
    return gulp.src([
        'app/**',
        'i18n/**',
        'package.json',
        'execute.js',
        'configuration.json'
    ], { base: '.' })
        // Minifies only if it's a JavaScript file
        .pipe(gulpIf('app/**/*.js', uglify()))
        .pipe(gulp.dest(temp));
});

/**
 * Creación del paquete ejecutable mediante JXCore
 */
gulp.task('package', function (cb) {
    exec('"c:\\Progra~2\\JXcore\\jx.exe" package execute.js ' + appName + ' -native --version ' + appVersion + ' --slim "app/statics/*,node?modules,logs/*,temp/*"', { cwd: temp }, function (err, stdout, stderr) {

        console.log(stdout);
        console.log(stderr);

        cb(err);
    });
});

/**
 * Mover al distributable solo lo necesario. (la parte estatica, las traducciones, el package para la gestion de dependencias, la configuracion y el ejecutable ) 
 * Lo demas se ha incluido en el ejecutable
*/
gulp.task('distributeMove', ['package'], function (cb) {
    return gulp.src([
        temp + '/app/statics/**',
        temp + '/i18n/**',
        temp + '/package.json',
        temp + '/configuration.json',
        temp + '/*.exe '
    ], { base: temp })
        .pipe(gulp.dest(dist));
});

/**
 * Instalación de dependencias en la carpeta de distribucion
 */
gulp.task('distribute', ['distributeMove'], function (cb) {
    exec('npm install', { cwd: dist }, function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);

        cb(err);
    });
});
