Como montar el entorno paso a paso para que no se olvide!

*** Instalar nodejs para ejecución y jxcore para compilar (https://github.com/jxcore/jxcore-release)

*** Instalar Visual Studio Code


- Ejecutar instalación dependencias:

    npm install

- Instalar en vscode la extensión ESLint (Ctrl+shift+P o F1 -> Install Ext -> EsLint)

- Ejecutar el comando:

    npm install -g eslint


- En caso de querer hacer tests de carga ejecutar instalación:

    npm install -g loadtest

(Ejecución https://www.npmjs.com/package/loadtest)


- Compilación y distribución:

Ctrl+shift+P o F1 -> Run Task -> [Commando]

- Debug: 

F5 


- Guia de estilo basica para los temas de ext (Aún en pruebas!)

http://dox.codaxy.com/ext4-themes/Getting-Started


- Documentación de express con mas información sobre el servidor:

http://expressjs.com/es/api.html