var requireDir = require('require-dir');

/**
 * Cargador para las rutas dinámicas
 */
var RouterLoader = Class.extend({

    controllerList: [],

    /**
     * Carga la lista con todas las rutas disponibles.
     *
     * Una ruta es un objeto XxxRoute almacenado en la carpeta ./controllers que crea y retorna un objeto express.Router
     *
     *
     */
    init: function (path) {

        this.controllerList = requireDir(path);
    },
    /**
     * La configuracion se basa en crear una instancia de cada uno de los objetos detectados y realizar la llamada a su metodo configure.
     *
     * @param app
     */
    configure: function (app) {

        for (var idx in this.controllerList) {
            var elm = this.controllerList[idx];
            var route = this.createController(elm, []);
            var router = route.configure();
            if (router) {
                app.use(router);
            }
        }
    },

    /**
     * Instancia una ruta
     * @param handler
     * @param params
     * @returns {*}
     */
    createController: function (Handler, params) {
        var obj = new Handler(params);
        return obj;
    }

});

module.exports = RouterLoader;