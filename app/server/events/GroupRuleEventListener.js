var moment = require('moment');
var dateformat = require('dateformat');

var GroupRuleEventListener = {
    init: function () {
        global.eventHandler.listen(["#in#"], this.track, this);
        global.eventHandler.listen(["#out#"], this.track, this);
    },
    /**
     * Aplica las reglas de entrada y salida para notificar a los usuarios
     */
    track: function (event) {
        var self = this;
        var GroupConfig = require('app/server/model/GroupConfig.js');
        var TrackingAlert = require('app/server/model/TrackingAlert.js');
        var dao = new GroupConfig.Dao();
        dao.loadFilteredData({ group_id: event.user.group_id }, function (err, rows) {
            if (err) return callback(err);

            var currentTrackTime = moment.utc(event.data.date, 'YYYY-MM-DDTHH:mm:ss');

            var cfg = rows[0];
            var morningStart = moment.utc(cfg.morning_start, 'HH:mm', true);
            var morningEnd = moment.utc(cfg.morning_end, 'HH:mm', true);
            var lateStart = moment.utc(cfg.afternoon_start, 'HH:mm', true);
            var lateEnd = moment.utc(cfg.afternoon_end, 'HH:mm', true);

            var morningStartInRange = self.checkTimeInRange(cfg.lapse_time, morningStart.clone(), currentTrackTime.clone());
            var morningEndInRange = self.checkTimeInRange(cfg.lapse_time, morningEnd.clone(), currentTrackTime.clone());
            var afternoonStartInRange = self.checkTimeInRange(cfg.lapse_time, lateStart.clone(), currentTrackTime.clone());
            var afternoonEndInRange = self.checkTimeInRange(cfg.lapse_time, lateEnd.clone(), currentTrackTime.clone());

            //Si alguno de los elementos esta en rango es true 
            if (!(morningStartInRange || morningEndInRange || afternoonStartInRange || afternoonEndInRange)) {
                //TODO crea un elemento track alert!
                var trkAlertDao = new TrackingAlert.Dao();
                var trackAlert = new TrackingAlert.Model();
                trackAlert.tracking_id = event.data.id;
                trackAlert.date = dateformat(new Date(), "yyyy-mm-dd'T'HH:MM:ss");
                trackAlert.sended = false;
                if (cfg.send_mail) {
                    trackAlert.sended = true;
                    self.sendNotification(cfg.address, event.user, event.data, trackAlert);
                }
                trkAlertDao.save(trackAlert, function (error, data) {
                    if (err) return console.log(error);
                    console.log('Generated trackAlert to user "' + event.user.username + '" because tracking at ' + dateformat(currentTrackTime, 'HH:MM', true));
                });
            }

        });
    },
    /**
     * Comprueba si la fecha se encuentra en rango con la fecha configurada y el lapso posible
     */
    checkTimeInRange: function (range, cfgTime, currentTime) {
        if (currentTime.clone().isBetween(cfgTime.clone().subtract(parseInt(range), 'minutes'), cfgTime.clone().add(parseInt(range), 'minutes'), 'minutes')) {
            return true;
        }

        return false;
    },
    /**
     * 
     */
    sendNotification: function (mailTo, user, track, trackAlert) {
        var self = this;

        var nodemailer = require('nodemailer');
        var smtpConfig = global.settings.getConfigValue('mailSender').smtpServerConfig;


        // create reusable transporter object using the default SMTP transport
        var transporter = nodemailer.createTransport(smtpConfig);
        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: global.settings.getConfigValue('mailSender').mailFrom, // sender address
            to: mailTo, // list of receivers
            subject: global.settings.getConfigValue('mailSender').mailSubject, // Subject line
            text: global.settings.getConfigValue('mailSender').mailText
                .replace('{username}', user.username)
                //.replace('{trackDate}', track.date)
                .replace('{trackDate}', dateformat(track.date, 'dd-mm-yyyy'))
                .replace('{trackHour}', dateformat(track.date, 'HH:MM', true))
                .replace('{trackType}', track.type)
                .replace('{trackAction}', track.action=='IN' ? "entrado" : "salido")
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: ' + info.response);
        });

    }
};

module.exports = GroupRuleEventListener;