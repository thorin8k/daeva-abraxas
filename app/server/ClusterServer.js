var http = require("http");


/**
 * Inicializa la escucha del server en modo cluster
 */
var ClusterServer = Class.extend({

    port: undefined,

    /**
     * Constructor por defecto.
     *
     * Se encarga de realizar la configuracion de puertos y de inicializar los procesos.
     *
     * Dispone de un parametro de configuración 'clustered' que se encarga de iniciar el servidor multiproceso o de forma simple.
     * Lo normal será utilizar el multiproceso en entornos de producción dejando el otro para desarrollos y pruebas.
     *
     *
     * @param process
     * @param port
     */
    init: function (process, port) {
        this.port = this.normalizePort(port || 3000);

        if (global.settings.getConfigValue("server").clustered === true) {
            this.initClustered();
        } else {
            this.initUnclustered();
        }
    },

    /**
     * Inicializa la clase server encargada del control de las solicitudes en forma multiproceso.
     *
     */
    initClustered: function () {
        var cluster = require("cluster");
        //Launch cluster
        if (cluster.isMaster) {
            //Count the machine's CPUs
            var cpuCount = require('os').cpus().length;

            //Create a worker for each CPU
            for (var idx = 0; idx < cpuCount; idx += 1) {
                cluster.fork();
            }

            //Listen for dying workers
            cluster.on('exit', function (worker) {

                //Replace the dead worker, we're not sentimental
                console.log('Worker %d died :(', worker.id);
                cluster.fork();

            });
        } else {
            this.initUnclustered();

        }
    },
    /**
     * Inicializa la clase server encargada del control de las solicitudes en un unico proceso.
     *
     */
    initUnclustered: function () {
        var self = this;
        //Initialize clustered servers
        var Server = require("app/server/Server.js");
        var srv = new Server();

        srv.port = this.port;
        //create http server
        var server = http.createServer(srv.app);
        //listen on provided ports
        server.listen(this.port);
        //add error handler
        server.on("error", function (err) {
            self.handleErrors(err, this.port);
        });
        //start listening on port
        server.on("listening", this.trazeStart);

        //TODO: create https server from config
    },

    /**
     * Controla los posibles errores de formato en el puerto
     * @param val
     * @returns {*}
     */
    normalizePort: function (val) {
        var port = parseInt(val, 10);

        if (isNaN(port)) {
            //named pipe
            return 3000;
        }

        if (port >= 0) {
            //port number
            return port;
        }

        return false;
    },
    /**
     * Gestiona los errores en el listen del servidor
     */
    handleErrors: function (error, port) {
        if (error.syscall !== "listen") {
            throw error;
        }

        var bind = typeof port === "string"
            ? "Pipe " + port
            : "Port " + port;

        //handle specific listen errors with friendly messages
        switch (error.code) {
            case "EACCES":
                console.error(bind + " requires elevated privileges");
                process.exit(1);
                break;
            case "EADDRINUSE":
                console.error(bind + " is already in use");
                process.exit(1);
                break;
            default:
                throw error;
        }
    },
    /**
     * Muestra el mensaje cuando el servidor inicia
     */
    trazeStart: function () {
        console.log('Server Worker running!');
    }

});

module.exports = ClusterServer;