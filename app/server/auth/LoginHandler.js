var path = require('path');
var express = require('express');
var JsonResponse = require('app/server/common/JsonResponse.js');

var LoginHandler = Class.extend({
    router: null,

    init: function () {
        this.router = express.Router();
    },
    configure: function () {
        this.router.use(this.check.bind(this.check));
        this.router.get('/login', this.login.bind(this.login));
        this.router.post('/login', this.loginPost.bind(this.loginPost));
        this.router.get('/logout', this.logout.bind(this.logout));


        return this.router;
    },
    /**
     * Comprueba que la sesion existe en caso de que la ruta que se solicita lo necesite.
     *
     * Metodo de comprobacion de los appkey cuando se implementen
     */
    check: function (request, response, next) {
        var url = require('url');
        var publicPaths = [
            '/', //Se pone como publico ya que es el punto base de acceso a la interfaz web
            '/login',
            '/webDirect',
            '/tracking/direct'
        ];
        if (publicPaths.indexOf(url.parse(request.url).pathname) !== -1 || request.session.username) { //TODO Check if registered app...

            return next();
        }
        response.redirect('./login');
    },

    /**
     * Muestra mensaje de Unauthorized en caso de que la sesion no exista.
     */
    login: function (request, response) {
        var jsRes = new JsonResponse();
        jsRes.success = false;
        jsRes.status = 403;
        jsRes.message = "Unauthorized";
        response.send(jsRes.toJson());
    },

    /**
     * Inicia la sesion en caso de recibir los parametros correctos
     */
    loginPost: function (request, response) {
        var jsRes = new JsonResponse();
        if (request.body.user) {
            var User = require('app/server/model/User.js');
            var usrDao = new User.Dao();
            usrDao.loadFilteredData({ username: request.body.user }, function (err, data) {

                jsRes.success = false;
                if (err) {
                    jsRes.status = 403;
                    jsRes.message = "Unauthorized. " + err;
                    response.send(jsRes.toJson());
                    return;
                }

                if (data[0] && data[0].password === request.body.pass) {
                    request.session.username = request.body.user;
                    request.session.user = data[0];
                    jsRes.success = true;
                } else {
                    jsRes.status = 403;
                }
                response.send(jsRes.toJson());
            });
        } else {
            jsRes.success = false;
            response.send(jsRes.toJson());
        }
    },
    /**
     * Finaliza la sesion actual
     */
    logout: function (request, response) {
        var sess = request.session;
        if (sess) {
            sess.destroy(function () {
                response.redirect('./');
            });
        }
    }

});

module.exports = LoginHandler;