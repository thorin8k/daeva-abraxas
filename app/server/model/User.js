var Model = function () {
    return {
        id: null,
        code: null,
        username: null,
        password: null,
        email: null,
        status: null,
        active: null,
        group_id: null,
        profile_id: null,
        code_password: null,
        name :null,
        surname :null,
        dni :null,
        address :null,
        phone :null,
        naf : null
    };
};

var BaseDao = require('app/server/model/BaseDao.js');
var Dao = BaseDao.extend({
    tableName: 'user',
    loadByCode: function (id, callback) {
        var qry = global.querys.getQuery(this.tableName, "qry_by_code");
        global.con.query(qry, id, callback);
    },
    loadByUsername: function (id, callback) {
        var qry = global.querys.getQuery(this.tableName, "qry_by_username");
        global.con.query(qry, id, callback);
    },
    isCodeUse: function (code, callback) {
        var qry = global.querys.getQuery(this.tableName, "is_code_use");
        global.con.query(qry, code, callback);
    }
});

module.exports = { Model: Model, Dao: Dao };