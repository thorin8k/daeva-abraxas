var Model = function () {
    return {
        id: null,
        group_id: null,
        morning_start: null,
        morning_end: null,
        afternoon_start: null,
        afternoon_end: null,
        lapse_time: null,
        send_mail: null,
        address: null
    };
};

var BaseDao = require('app/server/model/BaseDao.js');
var Dao = BaseDao.extend({
    tableName: 'group_config'
});

module.exports = { Model: Model, Dao: Dao };