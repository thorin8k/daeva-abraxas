var Model = function() {
    return {
        id: null,
        date: null,
        tracking_id: null,
        sended: null,
        resolved: null

    };
};
var BaseDao = require('app/server/model/BaseDao.js');
var Dao = BaseDao.extend({
    tableName: 'tracking_alert'

});

module.exports = { Model: Model, Dao: Dao };