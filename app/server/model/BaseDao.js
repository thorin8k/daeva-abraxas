var BaseModel = Class.extend({

    loadAllData: function (callback) {
        var qry = global.querys.getQuery(this.tableName, "qry_all");
        global.con.query(qry, callback);
    },
    loadFilteredData: function (filters, callback) {
        var qry = global.querys.getQuery(this.tableName, "qry_filtered");
        var stringFilters = global.querys.parseFilters(filters);
        qry = qry.replace('{filters}', stringFilters);
        global.con.query(qry, callback);
    },
    loadById: function (id, callback) {
        var qry = global.querys.getQuery(this.tableName, "qry_by_id");
        global.con.query(qry, id, callback);
    },
    save: function (object, callback) {
        var qry = global.querys.getQuery(this.tableName, "insert");
        global.con.query(qry, [object], callback);
        //TODO: implementar historico
        // global.con.queryHist(qry, [object], this.tableName, object.id, null, 'INSERT', object, callback);
    },
    update: function (objectId, newObject, callback) {
        var self = this;
        this.loadById(objectId, function (err, row) {
            if (err) {
                return callback(err);
            }
            newObject.id = objectId;
            var values = {
                id: objectId,
                newVal: newObject,
                old: row[0]
            };
            var qry = global.querys.getQuery(self.tableName, "update");
            global.con.query(qry, [values.newVal, values.id], callback);

            //TODO: implementar historico
            // global.con.queryHist(qry, [values.newVal, values.id], self.tableName, values.id, values.old, 'UPDATE', values.newVal, callback);
        });
    },
    remove: function (objectId, callback) {
        var self = this;
        self.loadById(objectId, function (err, row) {
            if (err) return err;
            var qry = global.querys.getQuery(self.tableName, "delete");
            global.con.query(qry, [objectId], callback);
            //TODO: implementar historico
            // global.con.queryHist(qry, [objectId], self.tableName, objectId, row[0], 'DELETE', null, callback);
        });
    }
});

module.exports = BaseModel;