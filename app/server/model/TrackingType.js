var Model = function () {
    return {
        id: null,
        name: null
    };
};

var BaseDao = require('app/server/model/BaseDao.js');
var Dao = BaseDao.extend({
    tableName: 'tracking_type'
});

module.exports = { Model: Model, Dao: Dao };