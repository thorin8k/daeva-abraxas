var Model = function () {
    return {
        id: null,
        name: null,
        active: null
    };
};


var BaseDao = require('app/server/model/BaseDao.js');
var Dao = BaseDao.extend({
    tableName: 'profile'
});

module.exports = { Model: Model, Dao: Dao };