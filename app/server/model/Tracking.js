var Model = function () {
    return {
        id: null,
        date: null,
        action: null,
        type_id: null,
        user_id: null,

    };
};
var BaseDao = require('app/server/model/BaseDao.js');
var Dao = BaseDao.extend({
    tableName: 'tracking',

    loadReportData: function (filters, callback) {
        //var qry = global.querys.getQuery(this.tableName, "qry_filtered");
        var qry = global.querys.getQuery(this.tableName, "qry_report");
        var stringFilters = global.querys.parseFilters(filters);
        qry = qry.replace('{filters}', stringFilters);
        global.con.query(qry, callback);
    },
});

module.exports = {Model: Model, Dao: Dao};