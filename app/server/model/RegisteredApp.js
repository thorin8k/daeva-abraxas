var Model = function () {
    return {
        id: null,
        name: null,
        app_key: null,
        active: null
    };
};

var BaseDao = require('app/server/model/BaseDao.js');
var Dao = BaseDao.extend({
    tableName: 'registered_app'
});

module.exports = { Model: Model, Dao: Dao };