var Model = function () {
    return {
        id: null,
        name: null,
        active: null,
        cif:null,
        account_quote_code:null,
        city:null,
        province:null,
        address:null,
        cp:null
    };
};

var BaseDao = require('app/server/model/BaseDao.js');
var Dao = BaseDao.extend({
    tableName: 'groups'
});

module.exports = { Model: Model, Dao: Dao };