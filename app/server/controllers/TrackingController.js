var path = require('path');
var express = require('express');
var JsonResponse = require('app/server/common/JsonResponse.js');
var TrackingService = require("app/server/service/TrackingService.js");
var dateformat = require('dateformat');

var TrackingController = Class.extend({
    router: null,

    init: function () {
        this.router = express.Router();
    },
    configure: function () {
        this.router.get('/tracking/list', this.listTrackings.bind(this.listTrackings));
        this.router.post('/tracking/:userId/in', this.trackIn.bind(this.trackIn));
        this.router.post('/tracking/:userId/out', this.trackOut.bind(this.trackOut));
        this.router.post('/tracking/direct', this.direct.bind(this.direct));
        this.router.get('/tracking/:userId/summary', this.timeSummary.bind(this.timeSummary));
        this.router.get('/tracking/:userId/eventList', this.eventList.bind(this.eventList));
        this.router.get('/tracking/eventList', this.eventList.bind(this.eventList));
        this.router.get('/tracking/:userId/periodTime', this.totalTime.bind(this.totalTime));

        return this.router;
    },
    /**
     * Lista los tracking de la aplicacion, es posible enviarle parametros de filtrado.
     *
     * Todavia no se ha definido la lista de parametros a utilizar para el filtrado.
     */
    listTrackings: function (request, response) {
        var self = this;
        var service = new TrackingService();
        var filters, limit, start;

        filters = request.query;
        service.list(filters, request, response, function (err, data) {
            var jsRes = new JsonResponse();
            jsRes.success = true;
            if (err) {
                jsRes.success = false;
                console.error(err);
            }
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    },
    /**
     * Registra una entrada para un usuario concreto. Es obligatorio pasar el identificador
     * del usuario.
     */
    trackIn: function (request, response) {
        var service = new TrackingService();
        var jsRes = new JsonResponse();
        //TODO validate ID
        service.in(request.params.userId, function (err, data) {
            jsRes.success = true;
            jsRes.data = data;
            if (err) {
                console.error(err);
                jsRes.success = false;
                jsRes.message = err;
            }
            response.send(jsRes.toJson());
        });
    },
    /**
     * Registra una salida para un usuario concreto. Es obligatorio pasar el identificador
     * del usuario.
     */
    trackOut: function (request, response) {
        var service = new TrackingService();
        var jsRes = new JsonResponse();
        //TODO validate ID
        service.out(request.params.userId, function (err, data) {
            jsRes.success = true;
            jsRes.data = data;
            if (err) {
                console.error(err);
                jsRes.success = false;
                jsRes.message = err;
            }
            response.send(jsRes.toJson());
        });
    },
    /**
         * Registra una entrada para un usuario concreto. Es obligatorio pasar el identificador
         * del usuario.
         */
    direct: function (request, response) {
        var service = new TrackingService();
        var jsRes = new JsonResponse();

        //TODO validate ID
        service.direct(request.query.userCode, function (err, data) {
            jsRes.success = true;
            jsRes.data = data;
            if (request.query.codePass != data.code_password){
                jsRes.success = false;
                jsRes.message = "Usuario y/o contraseña incorrectas";
                jsRes.data = null;
                response.send(jsRes.toJson());
            }
            if (err) {
                console.error(err);
                jsRes.success = false;
                jsRes.message = err;
                response.send(jsRes.toJson());
            }
            var newDate = new Date();
            newDate.setDate(newDate.getDate());

            service.getTotalTime(data.id, "day" , function (err, data) {
                jsRes.data = {
                    'user':jsRes.data,
                    'timeSumary':data,
                    'timeRegister':dateformat(newDate, "dd/mm/yyyy HH:MM")
                };
                response.send(jsRes.toJson());
            });


        });
    },

    /**
     * 
     */
    timeSummary: function (request, response) {
        var service = new TrackingService();
        var jsRes = new JsonResponse();

        service.timeSummary(request.params.userId, function (err, data) {
            jsRes.success = true;
            jsRes.data = data;
            if (err) {
                console.error(err);
                jsRes.success = false;
                jsRes.message = err;
            }
            response.send(jsRes.toJson());
        });
    },

    /**
     * 
     */
    eventList: function (request, response) {
        var service = new TrackingService();
        var jsRes = new JsonResponse();
        var params = {};
        // si es admin muestro todos los del grupo
        if (request.session.user.profile_id == global.constant.Admin){
            params = { group_id: request.session.user.group_id };
        }else{
            if (request.params.userId) {
                params = { user_id: request.params.userId };
            } else {
                params = { group_id: request.session.user.group_id };
            }
        }

        service.eventList(params, function (err, data) {
            jsRes.success = true;
            jsRes.data = data;
            if (err) {
                console.error(err);
                jsRes.success = false;
                jsRes.message = err;
            }
            response.send(jsRes.toJson());
        });
    },

    totalTime: function (request, response) {
        var service = new TrackingService();
        var jsRes = new JsonResponse();
        var period = request.query.period;
        service.getTotalTime(request.params.userId, period, function (err, data) {
            jsRes.success = true;
            jsRes.data = data;
            if (err) {
                console.error(err);
                jsRes.success = false;
                jsRes.message = err;
            }
            response.send(jsRes.toJson());
        });
    }
});

module.exports = TrackingController;