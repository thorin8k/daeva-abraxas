var path = require('path');
var express = require('express');
var JsonResponse = require('app/server/common/JsonResponse.js');
var fs = require('fs');
var dateformat = require('dateformat');

var ReportController = Class.extend({
    router: null,
    init: function () {
        this.router = express.Router();
    },
    configure: function () {
        this.router.get('/reports/:userId/timeReport', this.timeReport.bind(this.timeReport));

        return this.router;
    },
    /**
     * Obtiene el archivo html necesario para los clientes
     */
    timeReport: function (request, response) {
        //request.params.userId
        var TrackingService = require('app/server/service/TrackingService.js');
        var tService = new TrackingService();
        //var filters = { user_id: request.params.userId }
        var filters = {user_id: request.params.userId, dateF: request.query.fini, dateT: request.query.ffin};
        tService.listReportData(filters, function (err, data) {
            if (err) {
                response.end();
                console.log(err);
            }

            //var template = fs.readFileSync('app/server/templates/TimeReportTemplate.html', 'utf8');
            var template = fs.readFileSync('app/server/templates/report.html', 'utf8');
            var footerTemplate = fs.readFileSync('app/server/templates/footer.html', 'utf8');

            //var jsreport = require('jsreport-core')();
            //### CAMBIO!!!  PARA QUE PERMITA MODULOS EN FUNCIÓN (require('dateformat')
            var jsreport = require('jsreport-core')({tasks: {allowedModules: '*'}});

            //crear objeto, agrupar en una linea siendo maximo 4 inserciones de entrada salida

            var map = [];
            var mapDayRepeat = [];
            for (var idx = 0; idx < data.length; idx += 2) {
                var elm = data[idx];
                var nextEl = data[idx + 1];
                var obj = { //todo refactorizar a una especie de constructor, o inicializador o como le quieras llamar
                    title: elm.username,
                    user: elm.username,
                    user_id: elm.id,
                    nombre_grupo:elm.NombreGrupo,
                    dni:elm.dni,
                    account_quote_code:elm.account_quote_code,
                    cif:elm.cif,
                    localidad:elm.localidad,
                    fullname: elm.surname + ", " + elm.name,
                    naf: elm.naf
                };

                // agruparemos por fecha
                var mainDate = dateformat(elm.date, 'dd/mm/yyyy');
                var mainDay = dateformat(elm.date, 'dd');

                if (!map[mainDay]) {
                    map[mainDay] = [];

                    mapDayRepeat[mainDay] = 0;
                    obj.day = mainDay;
                }else {
                    obj = map[mainDay][0];
                    mapDayRepeat[mainDay] = mapDayRepeat[mainDay] + 1;
                }


                //En las entradas se establece el tiempo de inicio y el de fin a la fecha actual por si no hay fin
                if (elm.action === "IN") {
                    obj["start" + mapDayRepeat[mainDay]] = dateformat(elm.date, 'HH:MM', true);
                    obj["end" + mapDayRepeat[mainDay]] = "-";
                }
                //Si existe fin se establece el tiempo
                if (nextEl && nextEl.action === "OUT") {
                    obj["end" + mapDayRepeat[mainDay]] = dateformat(nextEl.date, 'HH:MM', true);
                }

                //Si existe fecha de fin se entra en la comprobacion de periodos:
                // en cambio al calculo de la pantalla de tiempos, en este simplemente creamos y adjuntamos al mapa un
                // elemento con la salida al dia siguiente, ya que no se necesiata y serian datos erroneos poner fichajes que no fueron realizados por el usuario
                // var tempEnd = obj["end" + mapDayRepeat[mainDay]];
                if (nextEl) { //controlar impares next element
                    var nextElMainDate = dateformat(nextEl.date, 'dd/mm/yyyy');
                    if (mainDate != nextElMainDate) {
                        var objNextDay = { //todo refactorizar a una especie de constructor, o inicializador o como le quieras llamar
                            title: elm.username,
                            user: elm.username,
                            user_id: elm.id,
                            nombre_grupo:elm.NombreGrupo,
                            account_quote_code:elm.account_quote_code,
                            cif:elm.cif,
                            localidad:elm.localidad,
                            fullname: elm.surname + ", " + elm.name,
                            naf: elm.naf
                        };
                        var nextday = dateformat(nextEl.date, 'dd');

                        map[nextday] = [];
                        mapDayRepeat[nextday] = 0;
                        objNextDay.day = nextday;
                        //al traer la consulta ordenada de base de datos ya se que va ser ele elmento 0 por lo que si cambia esta premisa se tiene que cambiar esto
                        obj["end" + mapDayRepeat[mainDay]] = "";
                        objNextDay["start0"] = ""; //Posible indicacion de comienzo de jornada dia anterior por si la cuadricula no queda del tod0 clara
                        objNextDay["end0"] = dateformat(nextEl.date, 'HH:MM', true);

                        mapDayRepeat[nextday] = 0;
                        map[nextday][0] = objNextDay;
                    }
                }

                map[mainDay][0] = obj;
            }

            // traspaso a un array simple
            var table = Object.keys(map).map(function(key) {
                return map[key][0]
            });
            // se ordena por dia
            table.sort(function(a, b){
                return a.day-b.day;
            });

            var head = table[0];
            if(head == undefined){
                head = table[1];
            }
            var d = new Date(request.query.ffin);
            var months = [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                "Julio", "Augosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ];
            head.month = months[d.getMonth()];
            head.year = d.getFullYear();

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth(); //January is 0!
            var yyyy = today.getFullYear();

            var today = dd+' de '+months[mm]+' de '+yyyy;
            head.today = today;





            jsreport.init().then(function () {
                return jsreport.render({
                    template: {
                        content: template,
                        // ## Función: traducción IN por ENTRADA y OUT por SALIDA
                        helpers: "function convertAction(str) { " +
                        "if (str=='IN') { " +
                        "str='ENTRADA';" +
                        "return str;" +
                        "} else { " +
                        "str= 'SALIDA'; " +
                        "return str; } " +
                        "}" +

                            // ## Función: formatea la fecha (dd/mm/yyyy HH:MM:ss)
                        "function parseDate(fecha) { " +
                        "var dateformat = require('dateformat');" +
                        "return dateformat(fecha, 'dd/mm/yyyy HH:MM:ss', true); " +
                        "}",

                        engine: 'handlebars',
                        recipe: 'phantom-pdf',
                        phantom: {
                            footerHeight: '180px',
                            footer: footerTemplate
                        }

                    },
                    data: {
                        //Información del usuario
                        info_usu: head, //data[0],
                        //Información de todas las entradas y salidas del usuario
                        trackings: table //data
                    }
                }).then(function (out) {
                    //prints pdf with headline Hello world
                    response.setHeader('Content-Type', 'application/pdf');
                    out.stream.pipe(response);
                });
            }).catch(function (e) {
                console.log(e);
            });
        });

    }

});

module.exports = ReportController;