var path = require('path');
var expressRouter = require('express').Router;
var JsonResponse = require('app/server/common/JsonResponse.js');
var GroupService = require("app/server/service/GroupService.js");
var GroupConfigService = require("app/server/service/GroupConfigService.js");

var GroupController = Class.extend({
    router: null,

    init: function () {
        this.router = expressRouter();
    },
    configure: function () {
        this.router.get('/group/list', this.listGroups.bind(this.listGroups));
        this.router.get('/group/:id', this.getGroup.bind(this.getGroup));
        this.router.get('/group/:id/config', this.getGroupConfig.bind(this.getGroupConfig));
        this.router.post('/group/:id/config', this.saveGroupConfig.bind(this.saveGroupConfig));
        this.router.post('/group', this.saveGroup.bind(this.saveGroup));
        this.router.post('/group/:id', this.saveGroup.bind(this.saveGroup));
        this.router.post('/group/:id/delete', this.deleteGroup.bind(this.deleteGroup));

        return this.router;
    },
    /**
     * Lista los grupos de la aplicacion, es posible enviarle parametros de filtrado.
     *
     * Todavia no se ha definido la lista de parametros a utilizar para el filtrado.
     */
    listGroups: function (request, response) {
        var self = this;
        var service = new GroupService();
        var filters, limit, start;

        filters = request.query;
        service.list(filters, request, response, function (err, data) {
            var jsRes = new JsonResponse();
            jsRes.success = true;
            if (err) {
                jsRes.success = false;
                console.error(err);
            }
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    },
    /**
     * Obtiene un grupo concreto utilizando su identificador
     */
    getGroup: function (request, response) {
        var self = this;
        var service = new GroupService();

        service.getById(request.params.id, function (err, data) {
            if (err) return console.error(err);

            var jsRes = new JsonResponse();
            jsRes.success = true;
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    },
    /**
     * Obtiene un grupo concreto utilizando su identificador
     */
    getGroupConfig: function (request, response) {
        var self = this;
        var service = new GroupConfigService();

        service.list({ group_id: request.params.id }, request, response, function (err, data) {
            if (err) return console.error(err);

            var jsRes = new JsonResponse();
            jsRes.success = true;
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    },
    /**
     * Llamada post para almacenar un grupo. Si se proporciona identificador se 
     * considera una actualizacion sino se dara de alta un nuevo grupo
     */
    saveGroupConfig: function (request, response) {
        var self = this;
        var service = new GroupConfigService();

        service.save(request.params.id, request.body, function (err, data) {
            if (err) return console.error(err);

            var jsRes = new JsonResponse();
            jsRes.success = true;
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    },
    /**
     * Llamada post para almacenar un grupo. Si se proporciona identificador se 
     * considera una actualizacion sino se dara de alta un nuevo grupo
     */
    saveGroup: function (request, response) {
        var self = this;
        var service = new GroupService();

        service.save(request.params.id, request.body, function (err, data) {
            if (err) return console.error(err);

            var jsRes = new JsonResponse();
            jsRes.success = true;
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    },
    /**
     * Llamada post para eliminar un grupo. Es necesario proporcionar el identificador
     */
    deleteGroup: function (request, response) {
        var self = this;
        var service = new GroupService();

        service.remove(request.params.id, function (err, data) {
            if (err) return console.error(err);

            var jsRes = new JsonResponse();
            jsRes.success = true;
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    }
});

module.exports = GroupController;