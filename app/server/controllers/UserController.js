var path = require('path');
var express = require('express');
var JsonResponse = require('app/server/common/JsonResponse.js');
var UserService = require("app/server/service/UserService.js");

var UserController = Class.extend({
    router: null,

    init: function () {
        this.router = express.Router();
    },
    configure: function () {
        this.router.get('/user/list', this.listUsers.bind(this.listUsers));
        this.router.get('/user/:id', this.getUser.bind(this.getUser));
        this.router.get('/user/status', this.userStatus.bind(this.userStatus));
        this.router.post('/user', this.saveUser.bind(this.saveUser));
        this.router.post('/user/:id', this.saveUser.bind(this.saveUser));
        this.router.post('/user/:id/validateCode', this.validateCode.bind(this.validateCode));
        this.router.post('/user/:id/validateUsername', this.validateUsername.bind(this.validateUsername));
        this.router.post('/user/:id/delete', this.deleteUser.bind(this.deleteUser));
        this.router.post('/user/:id/generateCode', this.generateCode.bind(this.generateCode));
        this.router.get('/session', this.getSession.bind(this.getSession));

        return this.router;
    },
    /**
     * Lista los usuarios en la aplicacion, es posible enviarle parametros de filtrado.
     *
     * Todavia no se ha definido la lista de parametros a utilizar para el filtrado.
     */
    listUsers: function (request, response) {
        var self = this;
        var service = new UserService();
        var filters, limit, start;

        filters = request.query;
        service.list(filters, request, response, function (err, data) {
            var jsRes = new JsonResponse();
            jsRes.success = true;
            if (err) {
                jsRes.success = false;
                console.error(err);
            }
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    },
    /**
     * Obtiene un usuario concreto utilizando su identificador
     */
    getUser: function (request, response) {
        var self = this;
        var service = new UserService();

        service.getById(request.params.id, function (err, data) {
            if (err) return console.error(err);

            var jsRes = new JsonResponse();
            jsRes.success = true;
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    },
    /**
     * Obtiene el estado de un usuario (true -> in | false -> out)
     */
    userStatus: function (request, response) {
        var service = new UserService();
        var jsRes = new JsonResponse();
        //TODO validate
        service.status(request.params.id, function (err, data) {
            jsRes.success = true;
            jsRes.data = data;
            if (err) {
                console.error(err);
                jsRes.success = false;
                jsRes.message = err;
            }
            response.send(jsRes.toJson());
        });
    },
    /**
     * Llamada post para almacenar un usuario. Si se proporciona identificador se 
     * considera una actualizacion sino se dara de alta un nuevo usuario
     */
    saveUser: function (request, response) {
        var service = new UserService();
        var jsRes = new JsonResponse();
        //TODO validate ID and parameters
        service.save(request.params.id, request.body, function (err, data) {
            jsRes.success = true;
            jsRes.data = data;
            if (err) {
                console.error(err);
                jsRes.success = false;
                jsRes.message = err;
            }
            response.send(jsRes.toJson());
        });
    },
    validateCode: function (request, response) {
        var service = new UserService();
        var jsRes = new JsonResponse();
        // userDao.loadById(request.params.id, function (err, row) {
        service.getByCode(request.body.code, function (error, data, callback) {

            if (data.length > 0) {
                if (data[0].id != request.params.id) {
                    jsRes.success = false;
                    response.send(jsRes.toJson());
                }
            }
            jsRes.success = true;
            response.send(jsRes.toJson());
        });
        // jsRes.success = true;
        // response.send(jsRes.toJson());
    },
    validateUsername: function (request, response) {
        var service = new UserService();
        var jsRes = new JsonResponse();
        // userDao.loadById(request.params.id, function (err, row) {
        service.getByUsername(request.body.username, function (error, data, callback) {

            if (data.length > 0) {
                if (data[0].id != request.params.id) {
                    jsRes.success = false;
                    response.send(jsRes.toJson());
                }
            }
            jsRes.success = true;
            response.send(jsRes.toJson());
        });
    },
    /**
     * Llamada post para eliminar un usuario. Es necesario proporcionar el identificador
     */
    deleteUser: function (request, response) {
        var service = new UserService();
        var jsRes = new JsonResponse();
        //TODO validate ID
        service.remove(request.params.id, function (err, data) {
            jsRes.success = true;
            jsRes.data = data;
            if (err) {
                console.error(err);
                jsRes.success = false;
                jsRes.message = err;
            }
            response.send(jsRes.toJson());
        });
    },
    generateCode: function (request, response) {
        var service = new UserService();
        var jsRes = new JsonResponse();
        //TODO validate ID


        service.generateCode(request.params.id, function (err, data) {
            jsRes.success = true;
            jsRes.data = data;
            if (err) {
                console.error(err);
                jsRes.success = false;
                jsRes.message = err;
            }
            response.send(jsRes.toJson());
        });
    },
    getSession: function (request, response) {
        var jsRes = new JsonResponse();
        //jsRes.status = 403;
        var Profile = require('app/server/model/Profile.js');
        var User = require('app/server/model/User.js');
        var usrDao = new User.Dao();
        var prlfDao = new Profile.Dao();
        usrDao.loadFilteredData({ username: request.session.username }, function (err, data) {
            jsRes.success = false;
            if (err) {
                jsRes.status = 403;
                jsRes.message = "Unauthorized. " + err;
                response.send(jsRes.toJson());
                return;
            }
            delete data[0].password;//Proteccion... no vaya ser
            prlfDao.loadById(data[0].profile_id, function (err2, data2) {
                if (err) {
                    console.log('Usuario sin perfil: ' + data[0].id);
                }
                data[0].profile = data2[0].name;

                jsRes.data = data;
                response.send(jsRes.toJson());
            });


        });
    }
});

module.exports = UserController;