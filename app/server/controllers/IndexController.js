var path = require('path');
var express = require('express');
var JsonResponse = require('app/server/common/JsonResponse.js');

var IndexController = Class.extend({
    router: null,
    init: function () {
        this.router = express.Router();
    },
    configure: function () {
        this.router.get('/', this.index.bind(this.index));
        this.router.get('/webDirect', this.webDirect.bind(this.webDirect));
        this.router.get('/translation', this.translation.bind(this.translation));
        this.router.get('/config', this.config.bind(this.config));

        return this.router;
    },
    /**
     * Obtiene el archivo html necesario para los clientes
     */
    index: function (request, response) {
        var filePath = path.resolve("app/statics/html/login.html");
        if (request.session.username) {
            filePath = path.resolve("app/statics/html/index.html");
        }

        response.sendFile(filePath);
    },
    /**
     * Obtiene el archivo html necesario para los clientes
     */
    webDirect: function (request, response) {
        var filePath = path.resolve("app/statics/html/webDirect.html");

        response.sendFile(filePath);
    },
    /**
     * Metodo interno para la obtencion de las traducciones.
     *
     * TODO es posible que sea necesario añadir esto a las urls publicas
     */
    translation: function (request, response) {
        var I18nLoader = require('app/server/common/i18nLoader.js');
        var i18 = new I18nLoader();
        i18.load(function (err, data) {
            if (err) throw err;
            var jsRes = new JsonResponse();
            jsRes.success = true;
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    },
    /**
     * Metodo interno para la obtencion de las configuraciones
     *
     * TODO es posible que sea necesario añadir esto a las urls publicas
     */
    config: function (request, response) {
        response.send(JSON.stringify(global.settings.currentSettings));
    }
});

module.exports = IndexController;