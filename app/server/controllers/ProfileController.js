var path = require('path');
var expressRouter = require('express').Router;
var JsonResponse = require('app/server/common/JsonResponse.js');
var ProfileService = require("app/server/service/ProfileService.js");

var ProfileController = Class.extend({
    router: null,

    init: function () {
        this.router = expressRouter();
    },
    configure: function () {
        this.router.get('/profile/list', this.listProfiles.bind(this.listProfiles));
        this.router.get('/profile/:id', this.getProfile.bind(this.getProfile));
        this.router.post('/profile/:id', this.saveProfile.bind(this.saveProfile));
        this.router.post('/profile/:id/delete', this.deleteProfile.bind(this.deleteProfile));

        return this.router;
    },
    /**
     * Lista los perfiles de la aplicacion, es posible enviarle parametros de filtrado.
     *
     * Todavia no se ha definido la lista de parametros a utilizar para el filtrado.
     */
    listProfiles: function (request, response) {
        var self = this;
        var service = new ProfileService();
        var filters, limit, start;

        filters = request.query;

        service.list(filters, request, response, function (err, data) {
            var jsRes = new JsonResponse();
            jsRes.success = true;
            if (err) {
                jsRes.success = false;
                console.error(err);
            }
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    },
    /**
     * Obtiene un perfil concreto utilizando su identificador
     */
    getProfile: function (request, response) {
        var self = this;
        var service = new ProfileService();

        service.getById(request.params.id, function (err, data) {
            if (err) return console.error(err);

            var jsRes = new JsonResponse();
            jsRes.success = true;
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    },
    /**
     * Llamada post para almacenar un perfil. Si se proporciona identificador se
     * considera una actualizacion sino se dara de alta un nuevo perfil
     */
    saveProfile: function (request, response) {
        var self = this;
        var service = new ProfileService();

        service.save(request.params.id, request.body, function (err, data) {
            if (err) return console.error(err);

            var jsRes = new JsonResponse();
            jsRes.success = true;
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    },
    /**
     * Llamada post para eliminar un perfil. Es necesario proporcionar el identificador
     */
    deleteProfile: function (request, response) {
        var self = this;
        var service = new ProfileService();

        service.delete(request.params.id, function (err, data) {
            if (err) return console.error(err);

            var jsRes = new JsonResponse();
            jsRes.success = true;
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    }
});

module.exports = ProfileController;