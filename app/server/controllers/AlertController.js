var path = require('path');
var expressRouter = require('express').Router;
var JsonResponse = require('app/server/common/JsonResponse.js');
var AlertService = require("app/server/service/AlertService.js");
var GroupConfigService = require("app/server/service/GroupConfigService.js");

var AlertController = Class.extend({
    router: null,

    init: function () {
        this.router = expressRouter();
    },
    configure: function () {
        this.router.get('/alerta/list', this.listAlerts.bind(this.listAlerts));
        this.router.post('/alerta/:id', this.saveAlert.bind(this.saveAlert));
        this.router.get('/alerta/:id', this.getAlert.bind(this.getAlert));

        return this.router;
    },
    /**
     * Lista alertas de la aplicacion
     */
    listAlerts: function (request, response) {
        var self = this;
        var service = new AlertService();
        var filters, limit, start;
        var session = request.session;

        filters = request.query;
        if(session.user.group_id != null)
            filters.group_id = session.user.group_id

        service.list(filters, request, response, function (err, data) {
            var jsRes = new JsonResponse();
            jsRes.success = true;
            if (err) {
                jsRes.success = false;
                console.error(err);
            }
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    },

    /**
     * Modifica el horario de una alerta
     * @param request
     * @param response
     */
    saveAlert: function (request, response) {
        var service = new AlertService();
        var jsRes = new JsonResponse();
        //TODO validate ID and parameters
        service.save(request.params.id, request.body, function (err, data) {
            jsRes.success = true;
            jsRes.data = data;
            if (err) {
                console.error(err);
                jsRes.success = false;
                jsRes.message = err;
            }
            response.send(jsRes.toJson());
        });
    },

    /**
     * Obtiene una alerta concreta utilizando su identificador
     */
    getAlert: function (request, response) {
        var self = this;
        var service = new AlertService();

        service.getById(request.params.id, function (err, data) {
            if (err) return console.error(err);

            var jsRes = new JsonResponse();
            jsRes.success = true;
            jsRes.data = data;
            response.send(jsRes.toJson());
        });
    }
});

module.exports = AlertController;
