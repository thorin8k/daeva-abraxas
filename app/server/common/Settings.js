var fs = require('fs');
var path = require('path');

var Settings = Class.extend({
    currentSettings: [],

    cfgFileName: 'configuration.json',


    databaseTableName: '',
    databaseKeyName: '',
    databaseValueName: '',
    /**
     *
     * @returns {Settings}
     */
    load: function () {
        //TODO utilizar nconf (proyecto de estadisticas)
        var rawData = fs.readFileSync(path.resolve(path.dirname(require.main.filename), this.cfgFileName), 'utf8');

        this.currentSettings = JSON.parse(rawData);
        return this;
    },

    /**
     *
     * @param key
     * @returns {string}
     */
    getConfigValue: function (key) {
        var record = this.currentSettings[key];
        if (record === null || record === undefined) {
            console.error('Value not configured: ' + key);
        }
        return record !== null ? record : '';
    },

    /**
     * Carga la tabla de configuración
     * 
     * //TODO utilizar nconf y evitar tener esto en base de datos
     */
    loadFromBD: function () {
        var self = this;
        if (this.databaseTableName != '') {
            var qry = global.querys.getQuery(this.databaseTableName, 'qry_all');
            if (qry != '') {
                global.con.query(qry, function (err, rows) {
                    if (err) throw err;
                    for (var idx = 0; idx < rows.length; idx++) {
                        var elm = rows[idx];
                        self.currentSettings[elm[self.databaseKeyName]] = elm[self.databaseValueName];
                    }
                });
            }
        }
    }

});

module.exports = Settings;