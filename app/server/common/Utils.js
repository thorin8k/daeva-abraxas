var Utils = {
    arrayToLower: function (mcArray) {
        var tmp = mcArray.join('~').toLowerCase();
        return tmp.split('~');
    },

    replaceAll: function (str, find, replace) {
        return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
    },

    encrypt: function (text) {
        var crypto = require('crypto');
        var algorithm = 'aes-256-ctr',
            password = 'd6F3Efeq';

        var cipher = crypto.createCipher(algorithm, password);
        var crypted = cipher.update(text, 'utf8', 'hex');
        crypted += cipher.final('hex');
        return new Buffer(crypted).toString('base64');
    },
    decrypt: function (text) {
        var crypto = require('crypto');
        var algorithm = 'aes-256-ctr',
            password = 'd6F3Efeq';

        var decipher = crypto.createDecipher(algorithm, password);
        var dec = decipher.update(new Buffer(text, 'base64').toString('ascii'), 'hex', 'utf8');
        dec += decipher.final('utf8');
        return dec;
    },
    rot13: function (data) {
        return data.replace(/[a-zA-Z]/g, function (c) {
            return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
        });
    },
    randomInt: function (low, high) {
        return Math.floor(Math.random() * (high - low + 1) + low);
    },
    leftPad: function (str, length) {
        str = str == null ? '' : String(str);
        length = ~~length;
        var pad = '';
        var padLength = length - str.length;

        while (padLength--) {
            pad += '0';
        }

        return pad + str;
    }
};

module.exports = Utils;