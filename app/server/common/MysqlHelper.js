var dateformat = require('dateformat');

function init() {
    global.con.queryHist = function (sql, values, entity, id_entity, old_obj, action, new_obj, fn) {
        global.con.query(sql, values, function (err, row) {
            if (err) throw err;

            var initial_data = '';
            var final_data = '';
            if (action === 'INSERT' && id_entity == undefined) {
                id_entity = row.insertId;
            }

            switch (action) {
                case 'INSERT':
                case 'UPDATE':
                    new_obj.id = row.insertId;
                    if (action === 'UPDATE') {
                        initial_data = JSON.stringify(old_obj);
                    }
                    final_data = JSON.stringify(new_obj);
                    var hist_obj = { action: action, date_reg: dateformat(new Date(), 'yyyymmddHHMMss'), table_name: entity, obj_identifier: id_entity, id_user: global.user };
                    saveHistoric(hist_obj, initial_data, final_data);
                    break;

                case 'DELETE':
                    initial_data = JSON.stringify(old_obj);
                    old_obj.status = 1;
                    final_data = JSON.stringify(old_obj);
                    var hist_obj = { action: action, date_reg: dateformat(new Date(), 'yyyymmddHHMMss'), table_name: entity, obj_identifier: id_entity, id_user: global.user };
                    saveHistoric(hist_obj, initial_data, final_data);
                    break;

            }
            if (fn) fn(null, row);
        });
    };
}

function saveHistoric(hist_obj, initial_data, final_data) {
    global.con.query('INSERT INTO historic SET ?', hist_obj, function (err, row) {
        if (err) {
            throw err;
        } else {
            var hist_data_obj = { initial_data: initial_data, final_data: final_data, id_historic: row.insertId };
            global.con.query('INSERT INTO historic_data SET ?', hist_data_obj, function (err, row) {
                if (err) {
                    throw err;
                }
            });
        }
    });
}

module.exports = init;