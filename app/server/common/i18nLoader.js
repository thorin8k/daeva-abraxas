var fs = require('fs');
var path = require('path');

var i18loader = Class.extend({
    /**
     *
     * @param lang
     * @param callback
     */
    load: function (callback) {
        var lang = global.settings.getConfigValue('lang');
        //TODO utilizar nconf (proyecto estadisticas)

        fs.readFile(path.dirname(require.main.filename) + '/i18n/lang_' + lang + ".json", 'utf8', function (err, data) {
            if (err) return callback(err, null);
            var parsedData = JSON.parse(data);

            callback(null, parsedData);
        });
    }

});

module.exports = i18loader;