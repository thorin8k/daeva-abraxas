var fs = require('fs');
var path = require('path');

var QueryHandler = Class.extend({
    querys: [],
    init: function () {
        var file = path.dirname(require.main.filename) + '/app/server/querys/mysql.app.querys.json';
        var data = fs.readFileSync(path.resolve(file), 'utf8');

        this.querys = JSON.parse(data);
    },
    getQuery: function (tableName, query) {
        return this.querys[tableName][query];
    },
    /**
     * Convierte un objeto clave valor en un conjunto de filtros.
     *
     * Mejorar aplicando un patron para las fechas.
     */
    parseFilters: function (filterObj) {
        var result = "";
        for (var idx in filterObj) {
            var elm = filterObj[idx];
            if (idx !== 'start' && idx !== 'limit') { //Se ignoran estos dos para utilizarlos en la paginacion
                if (elm !== '') {
                    var type = idx.substring(idx.length - 1);
                    if (type === 'F' || type === 'T') {
                        idx = idx.substring(0, idx.length - 1);
                        if (type === 'F') {
                            result += " AND " + idx + " >= '" + elm + "'";
                        } else if (type === 'T') {
                            result += " AND " + idx + " < '" + elm + "'";
                        }
                    } else {
                        var val = parseInt(elm);
                        if (!isNaN(val)) {
                            result += " AND " + idx + " = " + elm;
                        } else {
                            result += " AND " + idx + " LIKE '%" + elm + "%'";
                        }
                    }
                }
            }
        }
        return result;
    },
    /**
     * Se encarga de aplicarle la paginacion a una query
     */
    applyPagination: function (qry, start, limit) {
        if (!start || !limit) {
            return qry.replace('{pagination}', '');
        }
        return qry.replace('{pagination}', ' LIMIT ' + start + ', ' + limit);
    }
});
module.exports = QueryHandler;