var JsonResponse = Class.extend({
    data: null,
    status: null,
    total: null,
    message: '',
    success: null,

    toJson: function () {
        return JSON.stringify(this);
    }
});

module.exports = JsonResponse;