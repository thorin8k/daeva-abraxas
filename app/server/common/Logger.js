/**
 * Based on ivsgroup nwconsole: github.com/ivsgroup/nwconsole
 *
 * Modified by Iago
 */

var fs = require('fs'),
    util = require('util'),
    dateformat = require('dateformat'),
    path = require('path');


var nwconsole = Class.extend({

    /**
     * Configuración de loglevels
     */
    logLevels: {
        error: {
            file: 'error.log',
            fileStream: null
        },
        info: {
            file: 'info.log',
            fileStream: null
        },
        debug: {
            file: 'debug.log',
            fileStream: null
        }
    },

    /**
     * Max log file size
     */
    maxFileSize: 1024,

    /**
     *
     * @returns {nwconsole}
     */
    init: function () {
        var self = this;

        this.createWriteStreams();

        //Nota para el futuro:
        // Esto sobreescribe los metodos de console.log
        // Es necesario que la sitaxis se mantenga tal cual....
        (function () {
            var log = console.log;
            var error = console.error;
            var warn = console.warn;
            var info = console.info;

            console.log = function () {
                var args = Array.prototype.slice.call(arguments);
                log.apply(this, args);
                self._log('log', args);
            };
            console.error = function () {
                var args = Array.prototype.slice.call(arguments);
                log.apply(this, args);
                self._err_log('error', args);
            };
            console.warn = function () {
                var args = Array.prototype.slice.call(arguments);
                log.apply(this, args);
                self._log('warn', args);
            };
            console.info = function () {
                var args = Array.prototype.slice.call(arguments);
                log.apply(this, args);
                self._log('info', args);
            };
            console.debug = function () {
                if (global.settings.getConfigValue('debug')) {
                    var args = Array.prototype.slice.call(arguments);
                    //log.apply(this, args);
                    self._debug_log('debug', args);
                }
            };
        } ());

        return this;
    },
    /**
     * Metodo de inicializacion de los stream de escritura
     */
    createWriteStreams: function () {
        var self = this;
        fs.exists(path.join(path.dirname(require.main.filename), 'logs/'), function (exists) {
            if (!exists) {
                fs.mkdirSync(path.join(path.dirname(require.main.filename), 'logs/'));
            }

            for (var idx in self.logLevels) {
                var level = self.logLevels[idx];
                level.fileStream = fs.createWriteStream(path.join(path.dirname(require.main.filename), 'logs/' + level.file), { flags: 'a' });
            }

            //Configura el intervalo de renombrados para los archivos
            self.renameInterv = setInterval(function () {
                self.configureInterval();
            }, 10000);

        });


    },
    /**
     * Configura los intervalos de renombrados
     */
    configureInterval: function () {
        for (var idx in this.logLevels) {
            var level = this.logLevels[idx];
            this.performRename(level.fileStream, path.join(path.dirname(require.main.filename), 'logs/' + level.file));
        }
    },
    /**
     * Metodo para realizar el renombrado de los archivos de log en funcion de su tamaño
     * @param fileStream
     * @param filePath
     */
    performRename: function (fileStream, filePath) {
        var stats;
        try {
            stats = fs.statSync(filePath);
        } catch (err) {
            return;
        }
        var fileSizeInMegabytes = stats['size'] * 0.001;//Conversión de Bytes a KBytes
        if (fileSizeInMegabytes >= this.maxFileSize) {
            fileStream.close();
            var currentDate = dateformat(new Date(), 'yyyymmddHHMMss');
            fs.renameSync(filePath, filePath.replace('.log', '-' + currentDate + '.log'));
            fileStream = fs.createWriteStream(filePath, { flags: 'a' });

            //Nullify
            stats = currentDate = null;
        }
    },
    /**
     * Metodos para escribir el log, cada uno en un archivo
     *
     * @param level
     * @param args
     * @private
     */
    _log: function (level, args) {
        if (!this.logLevels.info.fileStream) return;

        var date = new Date();
        level = ('      ' + level).slice(-5);
        this.logLevels.info.fileStream.write('[' + dateformat(date, 'dd-mm-yyyy HH:MM:ss') + '] - ' + util.format.apply(null, args) + '\n');
    },
    _err_log: function (level, args) {
        if (!this.logLevels.error.fileStream) return;

        var date = new Date();
        level = ('      ' + level).slice(-5);
        this.logLevels.error.fileStream.write('[' + dateformat(date, 'dd-mm-yyyy HH:MM:ss') + '] - ' + util.format.apply(null, args) + '\n');
    },
    _debug_log: function (level, args) {
        if (!this.logLevels.debug.fileStream) return;

        var date = new Date();
        level = ('      ' + level).slice(-5);
        this.logLevels.debug.fileStream.write('[' + dateformat(date, 'dd-mm-yyyy HH:MM:ss') + '] - ' + util.format.apply(null, args) + '\n');
    }

});


module.exports = nwconsole;
