var helmet = require('helmet');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var express = require('express');
var session = require('express-session');
var compression = require('compression');
var cors = require('cors');
var FileStore = require('session-file-store')(session);
var RouterLoader = require('app/server/RouterLoader.js');
var dateformat = require('dateformat');

/**
 * Clase servidor encargada de configurar las rutas
 */
var Server = Class.extend({

    routeLoader: null,

    sessionManager: null,
    app: null,

    /**
     * Constructor por defecto que configura el cargador de rutas y las sesiones
     */
    init: function () {
        this.app = express();

        this.config();
        this.routes();
    },
    /**
     * Se encarga de realizar la configuración inicial del servidor
     */
    config: function () {
        //Security
        this.app.use(helmet());
        //mount json form parser
        this.app.use(bodyParser.json());
        //mount query string parser
        this.app.use(bodyParser.urlencoded({ extended: true }));
        //mount cookieParser
        this.app.use(cookieParser());
        // compress responses
        this.app.use(compression());
        //Enable cors to allow external references
        this.app.use(cors());
        //mount session
        this.app.use(session({
            store: new FileStore({
                path: global.settings.getConfigValue("server").sessionDir, //The dir cleans itself
                ttl: 2 * 60 * 60 * 24, //Tiempo en segundos hasta que caduca la sesion si no se usa
                logFn: function (err) {
                    if (err.indexOf('ENOENT') == -1) { //Ignorar los errores de destroy
                        console.error(err);
                    }
                },
                encrypt: true
            }),
            secret: 'password123456',
            // name: 'sessIdentifier',
            resave: true,
            saveUninitialized: true
        }));

        //add static paths
        this.app.use('/statics', express.static("app/statics"));

        //Autentication routes
        var authLoader = new RouterLoader('./auth');
        authLoader.configure(this.app);

        // catch 404 and forward to error handler
        this.app.use(function (err, req, res, next) {
            var error = new Error("Not Found");
            err.status = 404;
        });
    },

    /**
     * Crea el cargador automatico de rutas
     */
    routes: function () {
        var router = express.Router();
        router.use(function (request, response, next) {
            delete request.query._;
            console.log('Request received::. ' + request.url);
            next();
        });
        this.app.use(router);

        //create controllers
        this.routeLoader = new RouterLoader('./controllers');
        this.routeLoader.configure(this.app);

    }
});


module.exports = Server;