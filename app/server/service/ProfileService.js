var BaseService = require('app/server/service/BaseService.js');
var Profile = require('app/server/model/Profile.js');

var ProfileService = BaseService.extend({
    /**
     * Constructor para la inicializacion del mainDao
     */
    init: function () {
        this.mainDao = new Profile.Dao();
    },
    /**
     * Sobreescritura del metodo save del base service para establecer los
     * atributos del modelo
     */
    save: function (id, data, callback) {
        var profileM = new Profile.Model();
        profileM.name = data.name;
        profileM.active = 1;

        this._super(id, profileM, callback);
    },


    /**
     * Sobreescritura del metodo del base service para filtrado de listas
     */
    list: function (filters, request, response, callback) {
        if (request.session.user.profile_id === global.constant.User) {
            return callback('Unauthorized');
        }
        if (request.session.user.profile_id === global.constant.Admin) {
            filters['id'] = global.constant.User; //Los admin de una empresa solo pueden crear usuarios
        }
        this._super(filters, request, response, callback);
    }

});

module.exports = ProfileService;