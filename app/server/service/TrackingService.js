var BaseService = require('app/server/service/BaseService.js');
var Tracking = require('app/server/model/Tracking.js');
var User = require('app/server/model/User.js');
var dateformat = require('dateformat');
var moment = require('moment');


var TrackingService = BaseService.extend({
    /**
     * Constructor para la inicializacion del mainDao
     */
    init: function () {
        this.mainDao = new Tracking.Dao();
    },
    /**
     * Metodo para hacer check in a un usuario, se encarga de marcar al usuario
     * con status in y registrar la hora de entrada.
     */
    in: function (userId, callback) {
        var self = this;
        if (userId) {
            var userDao = new User.Dao();
            userDao.loadById(userId, function (error, data) {
                if (error) return callback(error, null);

                var user = data[0];
                user.status = 1;
                //TODO check user status, evitar introducir otro in si ya esta in
                userDao.update(userId, user, function (err, updatedData) {
                    if (err) return callback(err, null);
                    var track = new Tracking.Model();
                    track.date = dateformat(new Date(), "yyyy-mm-dd'T'HH:MM:ss");
                    track.action = 'IN';
                    track.type_id = 1;//DEFAULT
                    track.user_id = user.id;

                    //TODO comprobar que el anterior elemento no es un out

                    self.mainDao.save(track, function (subErr, insertedData) {
                        if (err) return callback(subErr, null);
                        delete user.password;
                        track.id = insertedData.insertId;

                        global.eventHandler.speak({
                            message: '#in#',
                            data: track,
                            user: user
                        });

                        callback(null, user);
                    });
                });
            });
        }
    },
    /**
     * Metodo para hacer check out a un usuario, se encarga de marcar al usuario
     * con status out y registrar la hora de salida
     */
    out: function (userId, callback) {
        var self = this;
        if (userId) {
            var userDao = new User.Dao();
            userDao.loadById(userId, function (error, data) {
                if (error) return callback(error, null);

                var user = data[0];
                //TODO check user status, evitar introducir otro out si ya esta out
                user.status = 0;
                userDao.update(userId, user, function (err, updatedData) {
                    if (err) return callback(err, null);
                    var track = new Tracking.Model();
                    track.date = dateformat(new Date(), "yyyy-mm-dd'T'HH:MM:ss");
                    track.action = 'OUT';
                    track.type_id = 1;//DEFAULT
                    track.user_id = user.id;

                    //TODO comprobar que el anterior elemento no es un out

                    self.mainDao.save(track, function (subErr, insertedData) {
                        if (err) return callback(subErr, null);
                        delete user.password;
                        track.id = insertedData.insertId;

                        global.eventHandler.speak({
                            message: '#out#',
                            data: track,
                            user: user
                        });

                        callback(null, user);
                    });
                });
            });
        }
    },

    /**
     * Realiza entradas o salidas dependiendo del estado del usuario mediante su codigo
     */
    direct: function (userCode, callback) {
        var self = this;
        if (userCode) {
            var userDao = new User.Dao();
            userDao.loadByCode(userCode, function (error, data) {
                if (error) return callback(error, null);

                var user = data[0];
                if (!user) return callback('User not found', null);
                if (user.status == 0) {
                    self.in(user.id, callback);
                } else {
                    self.out(user.id, callback);
                }
            });
        }
    },
    /**
     * Sobreescritura del metodo del base service para filtrado de listas
     */
    list: function (filters, request, response, callback) {
        //TODO aplicar filtro automatico segun el perfil del usuario
        this._super(filters, request, response, callback);
    },

    /**
     * TimeSummary creado para construir el calendario visualmente
     *
     * @param params
     * @param callback
     */
    eventList: function (params, callback) {
        var self = this;

        this.list(params, null, null, function (err, data) {
            if (err) {
                return callback(err, null);
            }
            var map = self.computeTimes(data);

            //Convierte el mapa obtenido en un objeto comprensible para mostrar en pantalla
            var response = [];
            for (var date in map) {
                var appoint = map[date];
                for (var elm in appoint) {
                    var obj = appoint[elm];
                    response.push(obj);
                }
            }

            callback(null, response);
        });
    },

    /**
     * TimeSummary creado para construir el scheduler visualmente
     *
     * @param userId
     * @param callback
     */
    timeSummary: function (userId, callback) {
        var self = this;
        if (userId) {
            this.list({ user_id: userId }, null, null, function (err, data) {

                if (err) {
                    return callback(err, null);
                }
                var response = [];
                var map = self.computeTimes(data);
                //Convierte el mapa obtenido en un objeto comprensible para mostrar en pantalla
                for (var date in map) {
                    var appoint = map[date];
                    for (var elm in appoint) {

                        var obj = appoint[elm];
                        obj.start = dateformat(obj.start, 'HH:MM', true);
                        obj.end = dateformat(obj.end, 'HH:MM', true);
                    }
                    response.push({ name: date, appointments: appoint });
                }
                callback(null, response);
            });
        }
    },
    /**
     * Metodo para calcular los dias intermedios entre dos tracking en error (no registrados el mismo dia la entrada y la salida)
     */
    addMiddleDays: function (diffDays, map, startDate) {
        for (var idx = 1; idx < diffDays; idx++) {
            var date = moment(startDate, 'DD/MM/YYYY').add(idx, 'days');
            map[dateformat(date.toDate(), 'dd/mm/yyyy')] = [];
            map[dateformat(date.toDate(), 'dd/mm/yyyy')].push({ start: dateformat(date.toDate(), 'yyyy-mm-dd') + "T00:00:00", end: dateformat(date.toDate(), 'yyyy-mm-dd') + "T23:59:00", 'class': 'wrong' });
        }
    },
    /**
     * Obtiene el tiempo total inputado para un usuario en un periodo concreto (week, month)
     */
    getTotalTime: function (userId, period, callback) {
        var self = this;
        if (userId) {
            var weekFirstDay = moment().startOf(period);
            var weekEndDay = moment().endOf(period);

            //todo al intentar hacer la consulta por dia encontre que no se tenia en cuenta el tiempo
            //todo (por lo tanto no se encontraba ninguna busqueda, añadirle el tiempo THH:MM:SS parece correcto pero revisar el resto de sitios por si no se queria
            var params = { user_id: userId, dateF: dateformat(weekFirstDay.toDate(), "yyyy-mm-dd'T'HH:MM:ss"), dateT: dateformat(weekEndDay.toDate(), "yyyy-mm-dd'T'HH:MM:ss") };


            this.list(params, null, null, function (err, data) {
                if (err) {
                    return callback(err, null);
                }
                var map = self.computeTimes(data);
                var response = 0;

                for (var date in map) {
                    var appoint = map[date];
                    for (var elm in appoint) {
                        var obj = appoint[elm];
                        // obj.start = dateformat(obj.start, 'HH:MM', true);
                        // obj.end = dateformat(obj.end, 'HH:MM', true);
                        var ini = moment(obj.start, 'YYYY-MM-DDTHH:mm:ss');
                        var to = moment(obj.end, 'YYYY-MM-DDTHH:mm:ss');
                        var duration = moment.duration(to.diff(ini));
                        response += duration.asHours();
                    }
                }

                callback(null, response.toFixed(2));
            });

        }
    },
    /**
     * Para unos tiempos entrada/salida recibidos los recorre y construye un objeto con los datos del usuario y la hora de entrada y salida.
     */
    computeTimes: function (data) {
        var map = [];
        if (data) { //TODO sacar a un metodo comun que obtenga un array de objetos
            for (var idx = 0; idx < data.length; idx += 2) {
                var elm = data[idx];
                var nextEl = data[idx + 1];

                //Para hacerlo multipaisano en la query de busqueda separo otro
                if (nextEl != undefined && elm.user_id != nextEl.user_id){
                    idx = idx  - 1;
                    nextEl = null;
                }


                var mainDate = dateformat(elm.date, 'dd/mm/yyyy');

                if (!map[mainDate]) {
                    map[mainDate] = [];
                }
                var obj = { title: elm.username, user: elm.username, user_id: elm.id };
                //En las entradas se establece el tiempo de inicio y el de fin a la fecha actual por si no hay fin
                if (elm.action === "IN") {
                    obj.start = elm.date;
                    obj.end = dateformat(new Date(), "yyyy-mm-dd'T'HH:MM:ss", false);
                }
                //Si existe fin se establece el tiempo
                if (nextEl && nextEl.action === "OUT") {
                    // por si se registran dos salidas, esto no deberia pasar a no ser que la lie el usuario con dos pestañas o navegadores si el siguiee elemento no lo tengo en cuenta

                    obj.end = nextEl.date;
                }

                //Si existe fecha de fin se entra en la comprobacion de periodos:
                // · Si existe una diferencia de dias entre la entrada y la salida mayor que 1 se introducen esos dias en el mapa
                // · Si la diferencia solo es de 1 dia se introduce un elemento adicional en cada registro con la fecha de fin para completar el tiempo entre entrada y salida
                var tempEnd = obj.end;
                if (tempEnd) {
                    var nextElMainDate = dateformat(tempEnd, 'dd/mm/yyyy');
                    var diff = moment(nextElMainDate, 'DD/MM/YYY').diff(moment(mainDate, 'DD/MM/YYY'), 'days');
                    if (diff > 1) {
                        this.addMiddleDays(diff, map, mainDate);
                    }
                    if (mainDate != nextElMainDate) {
                        obj.end = dateformat(obj.start, 'yyyy-mm-dd', false) + "T23:59:00";
                        obj.class = 'wrong';
                        var newObj = {};
                        if (!map[nextElMainDate]) {
                            map[nextElMainDate] = [];
                        }
                        newObj.start = dateformat(tempEnd, 'yyyy-mm-dd', false) + "T00:00:00";
                        newObj.end = tempEnd;
                        newObj.class = 'wrong';
                        map[nextElMainDate].push(newObj);
                    }
                }
                map[mainDate].push(obj);
            }
        }
        return map;
    },
    listReportData: function (filters, callback) {
        if (filters) {
            return this.mainDao.loadReportData(filters, callback);
        }
    }


});

module.exports = TrackingService;