var BaseService = require('app/server/service/BaseService.js');
var GroupConfig = require('app/server/model/GroupConfig.js');

var GroupConfigService = BaseService.extend({
    /**
     * Constructor para la inicializacion del mainDao
     */
    init: function () {
        this.mainDao = new GroupConfig.Dao();
    },
    /**
     * Sobreescritura del metodo del base service para filtrado de listas
     */
    list: function (filters, request, response, callback) {
        if (request.session.user.profile_id === global.constant.User) {
            return callback('Unauthorized');
        }
        if (request.session.user.profile_id === global.constant.Admin) {
            filters['id'] = request.session.user.group_id;
        }
        this._super(filters, request, response, callback);
    },
    save: function (id, data, callback) {
        var self = this;
        this.mainDao.loadFilteredData({ group_id: id }, function (err, rows) {
            if (err) return callback(err);

            var cfg = rows[0];
            if (cfg) {
                self.mainDao.update(cfg.id, data, callback);
            } else {
                //Create
                self.mainDao.save(data, callback);
            }
        });
    }

});

module.exports = GroupConfigService;