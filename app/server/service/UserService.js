var BaseService = require('app/server/service/BaseService.js');
var User = require('app/server/model/User.js');

var UserService = BaseService.extend({

    /**
     * Constructor para la inicializacion del mainDao
     */
    init: function () {
        this.mainDao = new User.Dao();
    },
    /**
     * Metodo para obtener el estado de un usuario.
     *
     * True si esta dentro false si esta fuera.
     */
    status: function (id, callback) {
        if (id) {
            this.mainDao.findById(id, function (error, data) {
                if (error) return callback(error, null);

                callback(null, data.status);
            });
        }
    },
    /**
     * Sobreescritura del metodo save del base service para establecer los atributos
     * del modelo.
     */
    save: function (id, data, callback) {
        var self = this;
        var override = this._super;

        var userDao = new User.Dao();
        if (id) {
            userDao.loadById(id, function (err, row) {
                if (err) {
                    return callback(err);
                }

                var userM = new User.Model();
                userM.username = data.username;
                if (data.password) {
                    userM.password = global.utils.rot13(data.password);
                } else {
                    userM.password = row[0].password;//Se deja la que ya tenia....
                }
                userM.email = data.email;
                userM.group_id = data.group_id;
                userM.profile_id = data.profile_id;
                userM.status = row[0].status;
                userM.active = 1;
                userM.name = data.name ;
                userM.surname = data.surname ;
                userM.dni = data.dni ;
                userM.address = data.address ;
                userM.phone = data.phone ;
                userM.code_password = data.code_password;
                userM.code = data.code;
                userM.naf = data.naf;

                override.call(self, id, userM, callback);
            });
        } else {
            if (data.password == undefined) {
                data.password = global.utils.rot13(data.password);
            }
            this._super(id, data, callback);
        }
    },


    /**
     * Sobreescritura del metodo del base service para filtrado de listas
     */
    list: function (filters, request, response, callback) {
        if (!filters) {
            filters = {};
        }
        if (request.session.user.profile_id === global.constant.Admin) {
            filters['group_id'] = request.session.user.group_id;
        }
        if (request.session.user.profile_id === global.constant.User) {
            return callback('Unauthorized');
        }
        this._super(filters, request, response, callback);
    },

    generateCode: function (userId, callback) {
        var self = this;
        var userDao = new User.Dao();
        userDao.loadById(userId, function (err, row) {
            if (err) {
                return callback(err);
            }
            var repeater = 1;



            //todo checkear que el codigo no exista ya no dejarlo tanto a lo random
             while (repeater >= 1){
                row[0].code = row[0].group_id + global.utils.leftPad(global.utils.randomInt(1, 999), 3);
                 userDao.loadByCode(row[0].code, function (err, data) {
                    console.log(data);
                    repeater = 3;
                });
            }

            userDao.update(userId, row[0], callback);
        });
    },

    getByCode: function(code, callback){
        return this.mainDao.loadByCode(code, callback);
    },

    getByUsername: function(username, callback){
        return this.mainDao.loadByCode(username, callback);
    },

    isCodeUse:function(code, callback){
        return this.mainDao.isCodeUse(code, callback);
    }
});

module.exports = UserService; 3