var BaseService = require('app/server/service/BaseService.js');
var Alert = require('app/server/model/TrackingAlert.js');

var AlertService = BaseService.extend({
    /**
     * Constructor para la inicializacion del mainDao
     */
    init: function () {
        this.mainDao = new Alert.Dao();
    },

    /**
     * Sobreescritura del metodo del base service para filtrado de listas
     */
    list: function (filters, request, response, callback) {
        /*
        if (request.session.user.profile_id === global.constant.User) {
            return callback('Unauthorized');
        }
        if (request.session.user.profile_id === global.constant.Admin) {
            filters['id'] = request.session.user.group_id;
        }
        */
        this._super(filters, request, response, callback);
    },

    save: function (id, data, callback) {
        var self = this;
        var override = this._super;

        var alertDao = new Alert.Dao();
        if (id) {
            alertDao.loadById(id, function (err, row) {
                if (err) {
                    return callback(err);
                }

                var alertM = new Alert.Model();
                alertM.date = data.date;
                alertM.tracking_id = row[0].tracking_id;
                alertM.sended = row[0].sended;
                alertM.resolved = 1;

                override.call(self, id, alertM, callback);
            });
        }
    }

});

module.exports = AlertService;
