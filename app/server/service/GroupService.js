var BaseService = require('app/server/service/BaseService.js');
var Group = require('app/server/model/Group.js');

var GroupService = BaseService.extend({
    /**
     * Constructor para la inicializacion del mainDao
     */
    init: function () {
        this.mainDao = new Group.Dao();
    },
    /**
     * Sobreescritura del metodo save del base service para establecer los
     * atributos del modelo
     */
    save: function (id, data, callback) {
        var groupM = new Group.Model();
        groupM.name = data.name;
        groupM.cif = data.cif;
        groupM.account_quote_code = data.account_quote_code;
        groupM.city = data.city ;
        groupM.province = data.province;
        groupM.address = data.address;
        groupM.cp = data.cp;
        groupM.active = 1;

        this._super(id, groupM, callback);
    },

    /**
     * Sobreescritura del metodo del base service para filtrado de listas
     */
    list: function (filters, request, response, callback) {
        if (request.session.user.profile_id === global.constant.User) {
            return callback('Unauthorized');
        }
        if (request.session.user.profile_id === global.constant.Admin) {
            filters['id'] = request.session.user.group_id;
        }
        this._super(filters, request, response, callback);
    }

});

module.exports = GroupService;