var BaseService = Class.extend({
    mainDao: null,
    /**
     * Obtencion de una lista de elementos.
     *
     * filters, start y limit son opcionales. Si no se pasan se devuelve todo (undefined);
     */
    list: function (filters, request, response, callback) {
        if (filters) {
            return this.mainDao.loadFilteredData(filters, callback);
        }
        this.mainDao.loadAllData(callback);
    },
    /**
     * Obtencion de un elemento mediante su identificador
     */
    getById: function (id, callback) {
        this.mainDao.loadById(id, callback);
    },
    /**
     * Metodo de creacion.
     *
     * Si el identificador se pasa como undefined se creara un nuevo elemento,
     * sino se modifica el correspondiente.
     */
    save: function (id, data, callback) {
        if (id) {
            //Update
            this.mainDao.update(id, data, callback);
        } else {
            //Create
            this.mainDao.save(data, callback);
        }
    },
    /**
     * Metodo de eliminado.
     *
     * El identificador es obligatorio para poder localizar el elemento a eliminar.
     */
    remove: function (id, callback) {
        if (id) {
            this.mainDao.remove(id, callback);
        }
    }
});

module.exports = BaseService;