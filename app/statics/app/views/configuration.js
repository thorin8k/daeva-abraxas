APP.AbraxasConfigView = Backbone.View.extend({

    template: _.template($('#configTemplate').html()),


    // populate the html to the dom
    render: function () {
        $('.mainMenu li').removeClass('active');
        $('#configMainMenuItem').parent().addClass('active');
        this.$el.html(this.template());
        return this;
    }

});