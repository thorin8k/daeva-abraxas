

APP.AbraxasGroupConfigView = Backbone.View.extend({

    template: _.template($('#groupConfigTemplate').html()),

    events: {
        "click #addGroup": "showAddModalWindow",
        "click .deleteGroup": "deleteElement"
    },

    // populate the html to the dom
    render: function () {
        $('.submenu a').removeClass('active');
        $('#groupMenuItem').addClass('active');
        this.$el.html(this.template());
        return this;
    },

    grid: null,
    loadGroupGrid: function () {
        var self = this;

        var Group = Backbone.Model.extend({});

        var Groups = Backbone.Collection.extend({
            model: Group,
            url: "/group/list",
            parse: function (response) {
                return response.data;
            },

            state: {
                pageSize: 15
            },
            mode: "client" // page entirely on the client side
        });

        var groupList = new Groups();

        var columns = [
            {
                name: "name",
                "data-title": "Nombre",
                title: "Nombre"
            },
            {
                name: "active",
                title: "Estado",
                "data-title": "Estado",
                index: true,
                sorttype: 'string',
                renderer: function (idx, object) {
                    return object.active ? "Activa" : "Inactiva";
                }
            },
            {
                name: "",
                title: "&nbsp;",
                actions: function (idx, object) {
                    if (object.active) {
                        return "<a href='#' elementIdentifier='" + object.id + "' class='deleteGroup'>Desactivar</a>";
                    }
                    return "";
                }
            }
        ];
        // Initialize a new Grid instance
        this.grid = new bbGrid.View({
            container: $('#groupGrid'),
            // rows: 15,
            // rowList: [15, 25, 50, 100],
            colModel: columns,
            collection: groupList,
            onRowClick: function (model) {
                if (!_.isEmpty(model)) {
                    self.showAddModalWindow();
                    $('#groupIdField').val(model.attributes.id);
                    $('#groupNameField').val(model.attributes.name);
                    $('#cifField').val(model.attributes.cif);
                    $('#account_quote_codeField').val(model.attributes.account_quote_code);

                    $('#cityField').val(model.attributes.city);
                    $('#provinceField').val(model.attributes.province);
                    $('#addressField').val(model.attributes.address);
                    $('#cpField').val(model.attributes.cp);
                }
            }
        });
        // Fetch some countries from the url
        groupList.fetch({ reset: true });
    },

    /**
     * Muestra la ventana modal con el formulario de usuarios
     */
    showAddModalWindow: function () {
        var self = this;
        $('#groupModal').modal();
        $('#addGroupForm')[0].reset();
        $('#groupIdField').val('');
        $('#addGroupButton').one("click", function () {
            if (!self.validateForm()) {
                return false;
            }
            var obj = {};
            $('#addGroupForm')
                .serializeArray()
                .map(function (x) {
                    obj[x.name] = x.value;
                });

            var groupIdentifier = $('#groupIdField').val();//TODO check if not "" and send to /group or /group/:id

            var url = "/group";
            if (groupIdentifier) {
                url += "/" + groupIdentifier;
            }
            $.ajax({
                url: url,
                method: 'post',
                data: obj,
                success: function (result) {
                    var resObj = JSON.parse(result);
                    $('#groupModal').modal('hide');
                    self.grid.collection.fetch({ reset: true });
                }
            });
        });
    },

    /**
     * Valida el formulario de insercion de usuarios
     */
    validateForm: function () {
        var form = $('#addGroupForm');
        form.find('.has-error').removeClass('has-error');

        var groupname = form.find('#groupNameField').val();
        var result = true;
        if (!groupname) {
            form.find('#groupNameField').parent().addClass('has-error');
            result = false;
        }

        return result;
    },

    /**
     * Elimina un usuario
     */
    deleteElement: function (evt) {
        var self = this;
        var elementIdentifier = $(evt.target).attr('elementidentifier');
        $('#confirm')
            .modal({ backdrop: 'static', keyboard: false })
            .one('click', '#delete', function (e) {
                //delete function
                $.ajax({
                    url: '/group/' + elementIdentifier + '/delete',
                    method: 'post',
                    success: function (result) {
                        self.grid.collection.fetch({ reset: true });
                    }
                });

            });
        evt.preventDefault();
    }
});