

APP.AbraxasVariousConfigView = Backbone.View.extend({

    template: _.template($('#variousConfigTemplate').html()),

    events: {
        "click #incidenceFix": "showAddModalWindow"
    },

    // populate the html to the dom
    render: function () {
        $('.submenu a').removeClass('active');
        $('#variousMenuItem').addClass('active');
        this.$el.html(this.template());

        return this;
    },
    grid: null,


    /**
     * Muestra la ventana modal con el formulario de usuarios
     */
    showAddModalWindow: function () {
        var self = this;
        $('#incidenceFixModal').modal();
        $('#incidenceFixForm')[0].reset();
        $('#addIncidenceFixButton').one("click", function () {
            if (!self.validateForm()) {
                return false;
            }
            var obj = {};
            $('#incidenceFixForm')
                .serializeArray()
                .map(function (x) {
                    obj[x.name] = x.value;
                });


            // var url = "/user";
            // if (userIdentifier) {
            //     url += "/" + userIdentifier;
            // }

            // $.ajax({
            //     url: url,
            //     method: 'post',
            //     data: obj,
            //     success: function (result) {
            //         var resObj = JSON.parse(result);
            //         $('#incidenceFixModal').modal('hide');
            //     }
            // });
        });
    }

});