APP.AbraxasTimetableConfigView = Backbone.View.extend({

    template: _.template($('#timeTableConfigTemplate').html()),

    events: {
        "change #groupSelectorField": "loadConfigData",
        "click #saveGroupConfig": "saveConfigData"
    },

    // populate the html to the dom
    render: function () {
        $('.submenu a').removeClass('active');
        $('#timeTableMenuItem').addClass('active');
        this.$el.html(this.template());
        return this;
    },
    grid: null,

    loadConfigData: function (evt) {
        $('#groupConfigForm')[0].reset();
        var value = evt;
        if (evt.target) {
            value = $("#" + evt.target.id).val();
        }
        if (value === "") {
            return false;
        }
        $.ajax({
            url: '/group/' + value + '/config',
            method: 'get',
            success: function (result) {
                var resObj = JSON.parse(result);
                if (resObj.success) {
                    $('#morningStart').val(resObj.data[0].morning_start);
                    $('#morningEnd').val(resObj.data[0].morning_end);
                    $('#afternoonStart').val(resObj.data[0].afternoon_start);
                    $('#afternoonEnd').val(resObj.data[0].afternoon_end);
                    $('#lapseTime').val(resObj.data[0].lapse_time);
                    $('#sendMail').prop('checked', resObj.data[0].send_mail);
                    $('#address').val(resObj.data[0].address);
                }
            }
        });

    },
    saveConfigData: function () {
        var groupId = $('#groupSelectorField').val();
        if (!groupId) {
            return false;
        }

        //IF VALIDATION
        if (this.validation()) {

            var grpConfig = {};
            grpConfig.morning_start = $('#morningStart').val();
            grpConfig.morning_end = $('#morningEnd').val();
            grpConfig.afternoon_start = $('#afternoonStart').val();
            grpConfig.afternoon_end = $('#afternoonEnd').val();
            grpConfig.lapse_time = $('#lapseTime').val();
            grpConfig.send_mail = $('#sendMail').prop('checked') ? 1 : 0;
            grpConfig.address = $('#address').val();
            grpConfig.group_id = groupId;

            $.ajax({
                url: '/group/' + groupId + '/config',
                method: 'post',
                data: grpConfig,
                success: function (result) {
                    var resObj = JSON.parse(result);
                    if (resObj.success) {
                        $('#successAlert').fadeIn().delay(2000).fadeOut();
                    }
                }
            });
        }
    },

    /**
     * Valida los campos del formulario
     * @returns {boolean}
     */
    validation: function () {

        var form = $('#groupConfigForm');
        form.find('.has-error').removeClass('has-error');

        var morningS = $('#morningStart').val();
        var morningE = $('#morningEnd').val();
        var afternoonS = $('#afternoonStart').val();
        var afternoonE = $('#afternoonEnd').val();
        var lapseTime = $('#lapseTime').val();


        //VALIDAR HORARIO MAÑANA: solo formato HH:MM
        if (((/^\d\d:\d\d$/.test(morningS)) || morningS.length == 0) && ((/^\d\d:\d\d$/.test(morningE)) || morningE.length == 0)) {
            $('#pnlHora').html('<span class="glyphicon glyphicon-calendar"></span> Horario').css('color', '#000');
        } else {
            $('#pnlHora').html('<span class="glyphicon glyphicon-calendar"></span> Horario de mañana no válido').css('color', '#b30000');
            form.find('#morningStart').parent().addClass('has-error');
            form.find('#morningEnd').parent().addClass('has-error');
            return;
        }

        //VALIDAR HORARIO TARDE: solo formato HH:MM
        if (((/^\d\d:\d\d$/.test(afternoonS)) || afternoonS.length == 0) && ((/^\d\d:\d\d$/.test(afternoonE)) || afternoonE.length == 0)) {
            $('#pnlHora').html('<span class="glyphicon glyphicon-calendar"></span> Horario').css('color', '#000');
        } else {
            $('#pnlHora').html('<span class="glyphicon glyphicon-calendar"></span> Horario de tarde no válido').css('color', '#b30000');
            form.find('#afternoonStart').parent().addClass('has-error');
            form.find('#afternoonEnd').parent().addClass('has-error');
            return;
        }

        //VALIDAR que los campos mañana o tarde esten cubiertos
        if ((morningS.length == 0 && morningE.length != 0) || (morningS.length != 0 && morningE.length == 0)
            || (afternoonS.length == 0 && afternoonE.length != 0) || (afternoonS.length != 0 && afternoonE.length == 0)) {
            $('#pnlHora').html('<span class="glyphicon glyphicon-calendar"></span> Horario no válido').css('color', '#b30000');
            form.find('#pnlHora').parent().addClass('has-error');
            return;
        } else {
            $('#pnlHora').html('<span class="glyphicon glyphicon-calendar"></span> Horario').css('color', '#000');
        }

        //VALIDAR que los rangos horarios sean los adecuados
        if (morningS.split(':') > morningE.split(':')) {
            $('#pnlHora').html('<span class="glyphicon glyphicon-calendar"></span> Horario no válido').css('color', '#b30000');
            form.find('#morningStart').parent().addClass('has-error');
            form.find('#morningEnd').parent().addClass('has-error');
            return;
        }
        if (afternoonS.split(':') > afternoonE.split(':')) {
            $('#pnlHora').html('<span class="glyphicon glyphicon-calendar"></span> Horario no válido').css('color', '#b30000');
            form.find('#afternoonStart').parent().addClass('has-error');
            form.find('#afternoonEnd').parent().addClass('has-error');
            return;
        }
        if (morningE.split(':') > afternoonS.split(':')) {
            $('#pnlHora').html('<span class="glyphicon glyphicon-calendar"></span> Horario no válido').css('color', '#b30000');
            form.find('#pnlHora').parent().addClass('has-error');
            return;
        }

        //VALIDAR QUE SEA UNA HORA CORRECTA: 00:00 - 23:59
        if (morningS.charAt(0) > 2 || morningE.charAt(0) > 2 || afternoonS.charAt(0) > 2 || afternoonE.charAt(0) > 2) {
            $('#pnlHora').html('<span class="glyphicon glyphicon-calendar"></span> Formato de hora no válido').css('color', '#b30000');
            form.find('#pnlHora').parent().addClass('has-error');
            return;
        }
        if ((morningS.charAt(0) == 2 && morningS.charAt(1) > 3)|| (morningE.charAt(0) == 2 && morningE.charAt(1) > 3)
            || (afternoonS.charAt(0) == 2 && afternoonS.charAt(1) > 3)|| (afternoonE.charAt(0) == 2 && afternoonE.charAt(1) > 3)) {

            $('#pnlHora').html('<span class="glyphicon glyphicon-calendar"></span> Formato de hora no válido').css('color', '#b30000');
            form.find('#pnlHora').parent().addClass('has-error');
            return;
        }
        if (morningS.charAt(3) > 5 || morningE.charAt(3) > 5 || afternoonS.charAt(3) > 5 || afternoonE.charAt(3) > 5) {
            $('#pnlHora').html('<span class="glyphicon glyphicon-calendar"></span> Formato de hora no válido').css('color', '#b30000');
            form.find('#pnlHora').parent().addClass('has-error');
            return;
        }


        //VALIDAR LAPSE TIME QUE SEA NUMÉRICO
        if (!/^([0-9])*$/.test(lapseTime)) {
            $('#lapseTime').val('No válido').css('color', '#b30000');
            form.find('#lapseTime').parent().addClass('has-error');
            return;
        } else {
            $('#lapseTime').html('<input type="text" class="form-control" id="lapseTime">').css('color', '#000');
        }


        //VALIDAR CHECK - CORREO
        //Si está marcado el check pero no se indica correo
        if ($('#sendMail').prop('checked') && ($('#address').val().length == 0)) {
            $('#address').val('Falta indicar cuenta de correo').css('color', '#b30000');
            form.find('#address').parent().addClass('has-error');
            return;
        }
        //Si está sin marcar el check pero hay escrito correo
        if (!$('#sendMail').prop('checked') && ($('#address').val().length != 0)) {
            $('#address').val('Marcar la casilla de verificación').css('color', '#b30000');
            form.find('#address').parent().addClass('has-error');
            return;
        }
        //Validar que sea una dirección de correo
        if ($('#address').val().length != 0) {
            if (!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/.test($('#address').val())) {
                $('#address').val('El mail no es correcto').css('color', '#b30000');
                form.find('#address').parent().addClass('has-error');
                return;
            }
        }

        //Si la validación ha sido correcta se devuelte true
        return true;
    }

});