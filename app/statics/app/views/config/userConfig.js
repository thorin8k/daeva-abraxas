

APP.AbraxasUserConfigView = Backbone.View.extend({

    template: _.template($('#userConfigTemplate').html()),

    events: {
        "click #addUser": "showAddModalWindow",
        "click .deleteElement": "deleteElement",
        "click .generateCode": "generateCode"
    },

    // populate the html to the dom
    render: function () {
        $('.submenu a').removeClass('active');
        $('#userMenuItem').addClass('active');
        this.$el.html(this.template());
        return this;
    },
    grid: null,
    loadUserGrid: function () {
        var self = this;
        var User = Backbone.Model.extend({});

        var Users = Backbone.Collection.extend({
            model: User,
            url: "/user/list",
            parse: function (response) {
                return response.data;
            },

            state: {
                pageSize: 15
            },
            mode: "client" // page entirely on the client side
        });

        var userList = new Users();

        var columns = [
            {
                name: "code",
                title: "Código",
                "data-title": "code",
                index: true,
                sorttype: 'string'
            },
            {
                name: "username",
                title: "Nombre",
                "data-title": "Name",
                index: true,
                sorttype: 'string'
            },
            {
                name: "email",
                title: "Email",
                "data-title": "Email",
                index: true,
                sorttype: 'string'
            },
            {
                name: "status",
                title: "Estado",
                "data-title": "Estado",
                index: true,
                sorttype: 'string',
                renderer: function (idx, object) {
                    return object.status ? "Dentro" : "Fuera";
                }
            },
            {
                name: "ProfileName",
                title: "Perfil",
                "data-title": "Perfil",
                index: true,
                sorttype: 'string'
            },
            {
                name: "GroupName",
                title: "Empresa",
                "data-title": "Empresa",
                index: true,
                sorttype: 'string'
            },
            {
                name: "active",
                title: "Estado",
                "data-title": "Estado",
                index: true,
                sorttype: 'string',
                renderer: function (idx, object) {
                    return object.active ? "Activo" : "Inactivo";
                }
            },
            {
                name: "",
                title: "&nbsp;",
                "data-title": "&nbsp;",
                actions: function (idx, object) {
                    var result = [];
                    // if (object.code === null) {
                    result.push("<a href='#' elementIdentifier='" + object.id + "' class='generateCode'>Generar Codigo</a>");
                    // }
                    if (object.active) {
                        result.push("<a href='#' elementIdentifier='" + object.id + "' class='deleteElement'>Desactivar</a>");
                    }
                    return result.length == 0 ? "" : result.join(' | ');
                }
            }
        ];

        // Initialize a new Grid instance
        this.grid = new bbGrid.View({
            container: $('#userGrid'),
            // rows: 15,
            // rowList: [15, 25, 50, 100],
            colModel: columns,
            collection: userList,
            onRowClick: function (model) {
                if (!_.isEmpty(model)) {
                    self.showAddModalWindow();
                    $('#userIdField').val(model.attributes.id);
                    $('#usernameField').val(model.attributes.username);
                    $('#mailField').val(model.attributes.email);
                    $('#groupSelectorField').val(model.attributes.group_id);
                    $('#profileSelectorField').val(model.attributes.profile_id);
                    $('#codeField').val(model.attributes.code);
                    $('#code_passwordField').val(model.attributes.code_password);
                    $('#nameField').val(model.attributes.name);
                    $('#surnameField').val(model.attributes.surname);
                    $('#dniField').val(model.attributes.dni);
                    $('#nafField').val(model.attributes.naf);
                    $('#addressField').val(model.attributes.address);
                    $('#phoneField').val(model.attributes.phone);
                }
            }
        });

        // Fetch some countries from the url
        userList.fetch({ reset: true });

        // $('#addUserButton').one("click", function () { // no entiendo por que esta asi, supongo que para prevenir el doble click crear varios usuarios, pero asi no se puede validar
            $('#addUserButton').click( function () {

            var valid = self.validateForm()
            if (!valid) {
                return false;
            }
            var obj = {};
            $('#addUserForm')
                .serializeArray()
                .map(function (x) {
                    obj[x.name] = x.value;
                });

            var userIdentifier = $('#userIdField').val();//TODO check if not "" and send to /user or /user/:id

            var url = "/user";
            if (userIdentifier) {
                url += "/" + userIdentifier;
            }
            if (obj.password1) {
                //Convert password
                obj.password = obj.password1;
                delete obj.password1;
                delete obj.password2;
            }
            $.ajax({
                url: url,
                method: 'post',
                data: obj,
                success: function (result) {
                    var resObj = JSON.parse(result);
                    $('#userModal').modal('hide');
                    self.grid.collection.fetch({ reset: true });
                }
            });
        });
    },
    /**
     * Muestra la ventana modal con el formulario de usuarios
     */
    showAddModalWindow: function () {
        var self = this;
        $('#userModal').modal();
        $('#addUserForm')[0].reset();
        $('#userIdField').val('');
        $('#addUserForm').find('.has-error').removeClass('has-error');
    },

    /**
     * Valida el formulario de insercion de usuarios
     */
    validateForm: function () {
        var form = $('#addUserForm');
        form.find('.has-error').removeClass('has-error');
        var result = true;
        var userIdentifier = $('#userIdField').val();

        var code = $('#codeField').val();
        var code_passwordField = $('#code_passwordField').val();
        var username = form.find('#usernameField').val();
        var name = $('#nameField').val();
        var surname = $('#surnameField').val();
        var dni = $('#dniField').val();
        var pass1 = form.find('#passwordField').val();
        var pass2 = form.find('#passwordRepeatField').val();
        var address = $('#addressField').val();
        var phone = $('#phoneField').val();
        var mail = form.find('#mailField').val();
        var profile_id = $('#profile_id').val();
        var group_id = $('#group_id').val();


        //VALIDAR CODE QUE SEA NUMÉRICO
        if (!/^([0-9])*$/.test(code)) {
            $('#codeField').val('Debe ser numérico').css('color', '#b30000');
            form.find('#codeField').parent().addClass('has-error');
            result = false;
        } else {
            $('#codeField').html('<input type="text" class="form-control" id="lapseTime">').css('color', '#000');
        }

        //VALIDAR CODE QUE SEA NUMÉRICO
        if (!/^([0-9])*$/.test(code)) {
            $('#code_passwordField').val('Debe ser numérico').css('color', '#b30000');
            form.find('#code_passwordField').parent().addClass('has-error');
            result = false;
        } else {
            $('#code_passwordField').html('<input type="text" class="form-control" id="lapseTime">').css('color', '#000');
        }


        if (!username) {
            form.find('#usernameField').parent().addClass('has-error');
            result = false;
        }
        if (!mail) {
            form.find('#mailField').parent().addClass('has-error');
            result = false;
        }
        //Si es una edicion la password puede enviarse vacia y no se cambiara 
        var userIdentifier = $('#userIdField').val();
        if (!userIdentifier) {
            if (!mail || !pass2 || pass1 !== pass2) {
                form.find('#passwordField').parent().addClass('has-error');
                form.find('#passwordRepeatField').parent().addClass('has-error');
                result = false;
            }
        }

        var obj = {};
        $('#addUserForm')
            .serializeArray()
            .map(function (x) {
                obj[x.name] = x.value;
            });
        var url = "/user";
        if (userIdentifier) {
            url += "/" + userIdentifier + "/validateCode";
        }

        $.ajax({
            url: url,
            method: 'post',
            async: false,
            data: obj,
            success: function (query) {
                var resObj = JSON.parse(query);
                if (resObj.success){
                    $('#codeField').html('<input type="text" class="form-control" id="lapseTime">').css('color', '#000');
                }else{
                    // $('#codeField').val('Elemento repetido').css('color', '#b30000');
                    $('#codeField').val('').css('color', '#b30000');
                    $('#codeField').attr("placeholder", "Ya Registrado");
                    form.find('#usernameField').parent().addClass('has-error');
                    form.find('#codeField').parent().addClass('has-error');
                    result = false;

                }
            }
        });
        var url = "/user";
        if (userIdentifier) {
            url += "/" + userIdentifier + "/validateUsername";
        }
        $.ajax({
            url: url,
            method: 'post',
            async: false,
            data: obj,
            success: function (query) {
                var resObj = JSON.parse(query);
                if (resObj.success){
                    $('#usernameField').html('<input type="text" class="form-control" id="lapseTime">').css('color', '#000');
                }else{
                    $('#usernameField').val('').css('color', '#b30000');
                    $('#usernameField').attr("placeholder", "Ya Registrado");
                    form.find('#usernameField').parent().addClass('has-error');
                    result = false;
                }
            }
        });
        return result;
    },

    /**
     * Elimina un usuario
     */
    deleteElement: function (evt) {
        var self = this;
        var elementIdentifier = $(evt.target).attr('elementidentifier');
        $('#confirm')
            .modal({ backdrop: 'static', keyboard: false })
            .one('click', '#delete', function (e) {
                //delete function
                $.ajax({
                    url: '/user/' + elementIdentifier + '/delete',
                    method: 'post',
                    success: function (result) {
                        self.grid.collection.fetch({ reset: true });
                    }
                });

            });
        evt.preventDefault();
    },
    generateCode: function (evt) {
        var self = this;
        var elementIdentifier = $(evt.target).attr('elementIdentifier');
        $('#confirm')
            .modal({ backdrop: 'static', keyboard: false })
            .one('click', '#delete', function (e) {
                //delete function
                $.ajax({
                    url: '/user/' + elementIdentifier + '/generateCode',
                    method: 'post',
                    success: function (result) {
                        self.grid.collection.fetch({ reset: true });
                    }
                });

            });
        evt.preventDefault();
    }
});