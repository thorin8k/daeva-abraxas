

APP.AbraxasProfileConfigView = Backbone.View.extend({

    template: _.template($('#profileConfigTemplate').html()),

    // populate the html to the dom
    render: function () {
        $('.submenu a').removeClass('active');
        $('#profileMenuItem').addClass('active');
        this.$el.html(this.template());
        return this;
    },
    loadProfileGrid: function () {
        var Profile = Backbone.Model.extend({});

        var Profiles = Backbone.Collection.extend({
            model: Profile,
            url: "/profile/list",
            parse: function (response) {
                return response.data;
            },

            state: {
                pageSize: 15
            },
            mode: "client" // page entirely on the client side
        });

        var profileList = new Profiles();

        var columns = [
            {
                name: "name",
                title: "Nombre",
                "data-title": "Nombre"
            }
        ];
        // Initialize a new Grid instance
        var grid = new bbGrid.View({
            container: $('#profileGrid'),
            // rows: 15,
            // rowList: [15, 25, 50, 100],
            colModel: columns,
            collection: profileList
        });

        // Fetch some countries from the url
        profileList.fetch({ reset: true });
    }
});