var UsersCombo = {

    load: function ($element) {
        var User = Backbone.Model.extend({});

        var Users = Backbone.Collection.extend({
            model: User,
            url: "/user/list",
            parse: function (response) {
                return response.data;
            },
            mode: "client" // page entirely on the client side
        });

        var UserView = Backbone.View.extend({
            tagName: "option",

            initialize: function () {
                _.bindAll(this, 'render');
            },
            render: function () {
                $(this.el).attr('value', this.model.get('id')).html(this.model.get('username'));
                return this;
            }
        });

        var UsersView = Backbone.View.extend({
            initialize: function () {
                _.bindAll(this, 'addOne', 'addAll');
                this.collection.bind('reset', this.addAll);
            },
            addOne: function (country) {
                $(this.el).append(
                    new UserView({ model: country }).render().el);
            },
            addAll: function () {
                this.collection.each(this.addOne);
            }
        });
        var users = new Users();
        new UsersView({ el: $element, collection: users });
        users.fetch({ reset: true, data: { "user.active": 1 } });
    }
};