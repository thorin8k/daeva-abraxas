var GroupsCombo = {

    load: function ($element, callback) {
        var Group = Backbone.Model.extend({});

        var Groups = Backbone.Collection.extend({
            model: Group,
            url: "/group/list",
            parse: function (response) {
                return response.data;
            },
            mode: "client" // page entirely on the client side
        });

        var GroupView = Backbone.View.extend({
            tagName: "option",

            initialize: function () {
                _.bindAll(this, 'render');
            },
            render: function () {
                $(this.el).attr('value', this.model.get('id')).html(this.model.get('name'));
                return this;
            }
        });

        var GroupsView = Backbone.View.extend({
            initialize: function () {
                _.bindAll(this, 'addOne', 'addAll');
                this.collection.bind('reset', this.addAll);
            },
            addOne: function (country) {
                $(this.el).append(
                    new GroupView({ model: country }).render().el);
            },
            addAll: function () {
                this.collection.each(this.addOne);
            }
        });
        var groups = new Groups();
        new GroupsView({ el: $element, collection: groups });
        groups.fetch({ reset: true, data: { active: 1 }, success: callback });
    }
};