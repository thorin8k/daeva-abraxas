var ProfilesCombo = {

    load: function ($element) {
        var Profile = Backbone.Model.extend({});

        var Profiles = Backbone.Collection.extend({
            model: Profile,
            url: "/profile/list",
            parse: function (response) {
                return response.data;
            },
            mode: "client" // page entirely on the client side
        });

        var ProfileView = Backbone.View.extend({
            tagName: "option",

            initialize: function () {
                _.bindAll(this, 'render');
            },
            render: function () {
                $(this.el).attr('value', this.model.get('id')).html(this.model.get('name'));
                return this;
            }
        });

        var ProfilesView = Backbone.View.extend({
            initialize: function () {
                _.bindAll(this, 'addOne', 'addAll');
                this.collection.bind('reset', this.addAll);
            },
            addOne: function (country) {
                $(this.el).append(
                    new ProfileView({ model: country }).render().el);
            },
            addAll: function () {
                this.collection.each(this.addOne);
            }
        });
        var profiles = new Profiles();
        var v = new ProfilesView({ el: $element, collection: profiles });
        profiles.fetch({ reset: true });
    }
};