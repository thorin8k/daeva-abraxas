APP.AbraxasTimeSummaryView = Backbone.View.extend({

    template: _.template($('#timeSummaryTemplate').html()),

    // populate the html to the dom
    render: function () {
        $('.submenu a').removeClass('active');
        $('#timeSummaryMenuItem').addClass('active');
        this.$el.html(this.template());
        return this;
    },


    createCalendar: function () {

        $.ajax({
            url: '/tracking/' + APP.viewport.user.id + '/summary',
            method: 'get',
            success: function (result) {
                var resObj = JSON.parse(result);

                var steps = [
                    '06:00',
                    '07:00',
                    '08:00',
                    '09:00',
                    '10:00',
                    '11:00',
                    '12:00',
                    '13:00',
                    '14:00',
                    '15:00',
                    '16:00',
                    '17:00',
                    '18:00',
                    '19:00',
                    '20:00',
                    '21:00'
                ];
                var $scheduler = $("#scheduler").schedulerjs({
                    'list': resObj.data,
                    'steps': steps,
                    'pixelsPerHour': 100,
                    'start': '09:30',
                    'end': '10:30',
                    'headName': 'Días'
                });
                $scheduler.schedulerjs('hideSelector');
            }
        });

        this.showPeriodTotalTime();
    },

    showPeriodTotalTime: function () {
        $.ajax({
            url: 'tracking/' + APP.viewport.user.id + '/periodTime',
            method: 'get',
            data: { period: 'month' },
            success: function (result) {
                var resObj = JSON.parse(result);
                $('#userPeriodTotalTime').html('Tiempo total este mes: <b>' + resObj.data + "</b> horas.");
            }
        });
    }
});