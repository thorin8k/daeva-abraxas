APP.AbraxasReportingView = Backbone.View.extend({

    template: _.template($('#ReportingTemplate').html()),

    events: {
        "click #printReport": "printReport"
    },
    // populate the html to the dom
    render: function () {
        $('.submenu a').removeClass('active');
        $('#reportingMenuItem').addClass('active');
        this.$el.html(this.template());

        return this;
    },

    validation: function () {

        //Valor identificador de usuario
        userIdentifier = $('#userSelectorField').val();
        //Si no se ha seleccionado usuario
        if (userIdentifier == null || userIdentifier.length == 0) {
            $('#lblUsuarios').html('<label class="control-label"  id="lblUsuarios" for="userSelectorField"><span class="glyphicon glyphicon-user"></span> Falta indicar usuario</label>').css('color', '#b30000');
            return;
        } else {
            $('#lblUsuarios').html('<label class="control-label"  id="lblUsuarios" for="userSelectorField"><span class="glyphicon glyphicon-user"></span> Usuario</label>').css('color', '#000');
        }


        //Valor fecha DESDE
        var dateObjectDesde = $("#datePickerDesde").datepicker('getDate');
        //fDesde = moment(dateObjectDesde).format("YYYY-MM-DD");
        //Si no se ha seleccionado fecha desde
        if (dateObjectDesde == null || dateObjectDesde.length == 0) {
            $('#datePickerDesde').val("Falta indicar fecha").css('color', '#b30000');
            return;
        } else {
            $('#datePickerDesde').css('color', '#000');
        }

        /*
        //Valor fecha HASTA
        var dateObjectHasta = $("#datePickerHasta").datepicker('getDate');
        fHasta = moment(dateObjectHasta).format("YYYY-MM-DD");
        //Si no se ha seleccionado fecha hasta
        if (dateObjectHasta == null || dateObjectHasta.length == 0) {
            $('#datePickerHasta').val("Falta indicar fecha").css('color', '#b30000');
            return;
        } else {
            $('#datePickerHasta').css('color', '#000');
        }

        //Si la fecha desde es mayor que la fecha hasta
        if (fDesde >= fHasta) {
            $('#lblFechas').html('<label class="control-label" id="lblFechas"> <span class="glyphicon glyphicon-calendar"></span> Rango de fechas no válido (la fecha "hasta" debe de ser posterior a la fecha "desde")</label>').css('color', '#b30000');
            return;
        } else {
            $('#lblFechas').html('<label class="control-label" id="lblFechas"> <span class="glyphicon glyphicon-calendar"></span> Rango de fechas</label>').css('color', 'black');
        }
        */

        var year1 = $("#datePickerDesde").datepicker('getDate').getFullYear();
        var month1 = $("#datePickerDesde").datepicker('getDate').getMonth() + 1;
        if (month1 < 10) {
            month1='0'+month1;
        }

        fDesde =year1+'-'+month1+'-01';

        if (month1 == 1 || month1 == 3 || month1 == 5 || month1 == 7 || month1 == 8 || month1 == 10 || month1 == 12) {
            fHasta=year1+'-'+month1+'-31';
        } else if (month1 == 4 || month1 == 6 || month1 == 9 || month1 == 11) {
            fHasta=year1+'-'+month1+'-30';
        } else {
                if ((year1 % 4 == 0) && ((year1 % 100 != 0) || (year1 % 400 == 0))) {
                    fHasta=year1+'-'+month1+'-29';
                } else {
                    fHasta=year1+'-'+month1+'-28';
                }
        }

        return true;
    },


    printReport: function () {

        if (this.validation()) {
            //window.open('/reports/' + APP.viewport.user.id + '/timeReport', '_blank');
            window.open('/reports/' + userIdentifier + '/timeReport?fini=' + fDesde + '&ffin=' + fHasta, '_blank');
        }

    }

});