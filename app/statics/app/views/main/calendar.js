APP.AbraxasCalendarView = Backbone.View.extend({

    template: _.template($('#calendarTemplate').html()),

    // populate the html to the dom
    render: function () {
        $('.submenu a').removeClass('active');
        $('#calendarMenuItem').addClass('active');
        this.$el.html(this.template());
        return this;
    },


    createCalendar: function (currentOnly) {

        var url = '/tracking/eventList';
        if (currentOnly) {
            url = '/tracking/' + APP.viewport.user.id + '/eventList';
        }
        $.ajax({
            url: url,
            method: 'get',
            success: function (result) {
                var resObj = JSON.parse(result);

                $('#calendar').fullCalendar({
                    dayClick: function (date, jsEvent, view) {
                        //alert('Clicked on: ' + date.format());
                    },
                    aspectRatio: 1.48,
                    //height: 'parent',
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: ''
                    },
                    locale: 'es',
                    defaultView: 'agendaWeek',
                    defaultDate: new Date(),
                    navLinks: false, // can click day/week names to navigate views
                    //editable: true,
                    //eventLimit: ue, // allow "more" link when too many events
                    events: resObj.data
                });

            }
        });


    }
});