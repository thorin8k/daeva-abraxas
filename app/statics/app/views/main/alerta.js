APP.AbraxasAlertView = Backbone.View.extend({


    template: _.template($('#alertTemplate').html()),


    events: {
        "click .resolved": "showAddModalWindow",
        "click .sended": "sendMail"
    },

    // populate the html to the dom
    render: function () {
        $('.submenu a').removeClass('active');
        $('#alertMenuItem').addClass('active');
        this.$el.html(this.template());


        return this;
    },

    grid: null,
    loadAlertGrid: function () {
        var self = this;

        var Alert = Backbone.Model.extend({});

        var Alerts = Backbone.Collection.extend({
            model: Alert,
            url: "/alerta/list",
            parse: function (response) {
                return response.data;
            },

            state: {
                pageSize: 15
            },
            mode: "client" // page entirely on the client side
        });

        var alertList = new Alerts();

        var columns = [
            {
                name: "date",
                "data-title": "Fecha",
                title: "Fecha",
                index: true,
                sorttype: 'string',

                renderer: function (idx, object) {
                    return jQuery.format.date(object.date,'dd/MM/yyyy HH:mm');
                },
            },
            {
                name: "action",
                "data-title": "Acción",
                title: "Acción",
                renderer: function (idx, object) {
                    return object.action=='IN' ? "ENTRADA" : "SALIDA";
                }
            },
            {
                name: "username",
                "data-title": "Usuario",
                title: "Usuario"
            },
            {
                name: "",
                title: "&nbsp;",
                "data-title": "&nbsp;",
                index: true,
                sorttype: 'string',
                //renderer: function (idx, object) {
                //    return object.resolved ? "Sí" : "No";
                //}
                actions: function (idx, object) {
                    var result = [];
                    if (!object.resolved) {
                        //return "<a href='#' elementIdentifier='" + object.id + "' class='resolved'>Resolver</a>";
                        result.push("<a href='#' elementIdentifier='" + object.id + "' class='resolved'> Resolver </a>");
                    }
                    if (!object.sended) {
                        //return "<a href='#' elementIdentifier='" + object.id + "' class='sended'>Enviar</a>";
                        result.push("<a href='#' elementIdentifier='" + object.id + "' class='sended'> Enviar </a>");
                    }

                    return result.length == 0 ? "" : result.join(' | ');
                }
            },
        ];
        // Initialize a new Grid instance
        this.grid = new bbGrid.View({
            container: $('#alertGrid'),
            // rows: 15,
            // rowList: [15, 25, 50, 100],
            colModel: columns,
            collection: alertList,

        });

        // Fetch some countries from the url
        alertList.fetch({ reset: true });
    },

    /**
     * Muestra la ventana modal para cambiar fecha y hora de una alerta
     */
    showAddModalWindow: function (evt) {
        var self = this;


        $('#alertModal').modal();

        var elementIdentifier = $(evt.target).attr('elementIdentifier');

        var url = "/alerta";
        if (elementIdentifier) {
            url += "/" + elementIdentifier;
        }

        $.ajax({
            url: url ,
            method: 'get',
            success: function (result) {
                var resObj = JSON.parse(result);
                if (resObj.success) {
                    $('#alertCurrentDateField').val(resObj.data[0].date.substring(0,10));
                    $('#alertCurrentHourField').val(resObj.data[0].date.substring(11,16));
                }
            }
        });

        $('#addAlertForm')[0].reset();


        $('#updateAlertButton').one("click", function () {

            if (!self.validateForm()) {
                return false;
            }

            //var obj = {};
            //$('#addAlertForm')
            //.serializeArray()
            //.map(function (x) {
            //obj[x.name] = x.value;
            //});

            var fechaModif=$('#alertModifDateField').val();
            var horaModif=$('#alertModifHourField').val();

            if ($('#alertModifDateField').val()== 0){
                fechaModif=$('#alertCurrentDateField').val();
            }
            if ($('#alertModifHourField').val()== 0){
                horaModif=$('#alertCurrentHourField').val();
            }


            var obj = {"date": fechaModif + 'T' + horaModif + ':00'};


            $.ajax({
            url: url,
            method: 'post',
            data: obj,
            success: function (result) {
                var resObj = JSON.parse(result);
                $('#alertModal').modal('hide');
                self.grid.collection.fetch({reset: true});
                }
            });
        });

    },

    /**
     * Envia mail con la alerta
     */
    sendMail: function (evt) {

    },

    /**
     * Valida los campos del formulario
     * @returns {boolean}
     */
    validateForm: function () {

        var form = $('#addAlertForm');
        form.find('.has-error').removeClass('has-error');

        //var date = $('#alertModifDateField').val();
        var hour = $('#alertModifHourField').val();
        var result=true;


        //VALIDAR HORARIO: solo formato HH:MM
        if (((/^\d\d:\d\d$/.test(hour)) || hour.length == 0) && ((/^\d\d:\d\d$/.test(hour)) || hour.length == 0)) {
            result=true;
        } else {
            form.find('#alertModifHourField').parent().addClass('has-error');
            result = false;
        }


        //VALIDAR QUE SEA UNA HORA CORRECTA: 00:00 - 23:59
        if (hour.charAt(0) > 2) {
            form.find('#alertModifHourField').parent().addClass('has-error');
            result = false;
        }
        if ((hour.charAt(0) == 2 && hour.charAt(1) > 3)) {
            form.find('#alertModifHourField').parent().addClass('has-error');
            result = false;
        }
        if (hour.charAt(3) > 5) {
            form.find('#alertModifHourField').parent().addClass('has-error');
            return false;
        }


        //Si la validación ha sido correcta se devuelte true
        return result;
    }

});