APP.AbraxasIndexView = Backbone.View.extend({

    template: _.template($('#indexTemplate').html()),

    // populate the html to the dom
    render: function () {
        $('.mainMenu li').removeClass('active');
        $('#trackingMainMenuItem').parent().addClass('active');
        this.$el.html(this.template());
        return this;
    }
});