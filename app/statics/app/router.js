// Load the application once the DOM is ready, using `jQuery.ready`:
APP.AbraxasRouter = Backbone.Router.extend({
    routes: {
        "index": "index",
        "index/timeSummary": "timeSummary",
        "index/calendar": "calendar",
        "index/reporting": "reporting",
        "configuration": "configuration",
        "configuration/users": "users",
        "configuration/profiles": "profiles",
        "configuration/groups": "groups",
        "configuration/timetable": "timeTable",

        "index/alerta": "alerta"

    },

    $container: $('#abraxasapp'),
    subContainer: '#innerAppContainer',

    initialize: function () {
        // this.collection = new APP.NoteCollection();
        // this.collection.fetch({ ajaxSync: false });
        // APP.helpers.debug(this.collection);
        this.index();
        this.timeSummary();
        // start backbone watching url changes
        Backbone.history.start();
    },

    index: function () {
        var view = new APP.AbraxasIndexView();
        this.$container.html(view.render().el);
    },
    timeSummary: function () {
        this.index();
        var v2 = new APP.AbraxasTimeSummaryView();
        $(this.subContainer).html(v2.render().el);
        v2.createCalendar();
    },
    calendar: function () {
        this.index();
        var v2 = new APP.AbraxasCalendarView();
        $(this.subContainer).html(v2.render().el);
        if (APP.viewport.user.profile === "Admin") {
            v2.createCalendar(false);
        } else {
            v2.createCalendar(true);
        }
    },
    reporting: function () {
        this.index();
        var v2 = new APP.AbraxasReportingView();
        $(this.subContainer).html(v2.render().el);

        UsersCombo.load($("#userSelectorField"));

        $('.datepicker').datepicker({
            language: 'es',
            locale: 'es',
            firstDay: 1
        });

        $('#datePickerHasta').datepicker({
            minDate: moment(), // Current day
            maxDate: moment().add(3, 'days')
        });

    },
    configuration: function () {
        var view = new APP.AbraxasConfigView();
        this.$container.html(view.render().el);
        if (APP.viewport.user.profile === "Admin") {
            $('#groupMenuItem').hide();
            $('#profileMenuItem').hide();
        }
    },
    users: function () {
        this.configuration();
        var v2 = new APP.AbraxasUserConfigView();
        $(this.subContainer).html(v2.render().el);

        v2.loadUserGrid();
        GroupsCombo.load($("#groupSelectorField"));
        ProfilesCombo.load($("#profileSelectorField"));

    },
    groups: function () {
        this.configuration();
        var v2 = new APP.AbraxasGroupConfigView();
        $(this.subContainer).html(v2.render().el);

        v2.loadGroupGrid();
    },
    profiles: function () {
        this.configuration();
        var v2 = new APP.AbraxasProfileConfigView();
        $(this.subContainer).html(v2.render().el);

        v2.loadProfileGrid();
    },
    various: function () {
        this.configuration();
        var v2 = new APP.AbraxasVariousConfigView();
        $(this.subContainer).html(v2.render().el);

    },
    timeTable: function () {
        this.configuration();
        var v2 = new APP.AbraxasTimetableConfigView();
        $(this.subContainer).html(v2.render().el);

        GroupsCombo.load($("#groupSelectorField"), function (records, data) {
            if (data.data.length === 1) {
                $("#groupSelectorField").val(data.data[0].id);
                v2.loadConfigData(data.data[0].id);
            }
        });

    },

    alerta: function () {
        this.index();
        var v2 = new APP.AbraxasAlertView();
        $(this.subContainer).html(v2.render().el);

        v2.loadAlertGrid();

        $('.datepicker').datepicker({
            language: 'es',
            locale: 'es',
            firstDay: 1
        });

        $('#datetimepicker3').datetimepicker({
            format: 'HH:mm'

        });

    }
});
