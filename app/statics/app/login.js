// An example Backbone application contributed by
// [Jérôme Gravel-Niquet](http://jgn.me/). This demo uses a simple
// [LocalStorage adapter](backbone.localStorage.html)
// to persist Backbone models within your browser.

// Load the application once the DOM is ready, using `jQuery.ready`:
$(function () {


    // The Application
    // ---------------

    // Our overall **AppView** is the top-level piece of UI.
    var AppView = Backbone.View.extend({

        // Instead of generating a new element, bind to the existing skeleton of
        // the App already present in the HTML.
        el: $("#loginPanel"),

        // Our template for the line of statistics at the bottom of the app.
        // statsTemplate: _.template($('#stats-template').html()),

        // Delegated events for creating new items, and clearing completed ones.
        events: {
            'click .loginButton': 'submitLogin',
            'click .directAccessButton': 'directAccessRedirect'
        },

        // At initialization we bind to the relevant events on the `Todos`
        // collection, when items are added or changed. Kick things off by
        // loading any preexisting todos that might be saved in *localStorage*.
        initialize: function () {

        },

        // Re-rendering the App just means refreshing the statistics -- the rest
        // of the app doesn't change.
        render: function () {

        },

        submitLogin: function (e) {
            var pass = this.$el.find('#pass').val();
            this.$el.find('#pass').val(rot13(pass));

            $.ajax({
                type: "POST",
                url: "./login",
                data: this.$el.find('form').serialize(), // serializes the form's elements.
                success: function (data) {
                    var response = JSON.parse(data);
                    if (response.success) {
                        window.location.assign('/');
                    } else {
                        $('.err').html('¿Problemas de memoria?<br/> Intentalo de nuevo ...');
                    }
                }
            });
            e.preventDefault();
        },
        directAccessRedirect: function (e) {
            window.location = "/webDirect";
            e.preventDefault();
        }

    });

    // Finally, we kick things off by creating the **App**.
    var App = new AppView();
});
