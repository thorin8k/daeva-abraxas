// An example Backbone application contributed by
// [Jérôme Gravel-Niquet](http://jgn.me/). This demo uses a simple
// [LocalStorage adapter](backbone.localStorage.html)
// to persist Backbone models within your browser.

// Load the application once the DOM is ready, using `jQuery.ready`:
$(function () {


    // The Application
    // ---------------

    // Our overall **AppView** is the top-level piece of UI.
    var AppView = Backbone.View.extend({

        // Instead of generating a new element, bind to the existing skeleton of
        // the App already present in the HTML.
        el: $("#abraxasapp"),

        // Our template for the line of statistics at the bottom of the app.
        // statsTemplate: _.template($('#stats-template').html()),

        // Delegated events for creating new items, and clearing completed ones.
        events: {
            "click .directEntrance": "directAccess",
            "click .cancelEntrance": "cancelAccess"

        },

        // At initialization we bind to the relevant events on the `Todos`
        // collection, when items are added or changed. Kick things off by
        // loading any preexisting todos that might be saved in *localStorage*.
        initialize: function () {
            this.showClock();
        },

        // Re-rendering the App just means refreshing the statistics -- the rest
        // of the app doesn't change.
        render: function () {

        },

        directAccessRedirect: function (e) {
        },

        showClock: function () {
            // Create two variable with the names of the months and days in an array
            var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
            var dayNames = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"]

            // Create a newDate() object
            var newDate = new Date();
            // Extract the current date from Date object
            newDate.setDate(newDate.getDate());
            // Output the day, date, month and year   
            $('#Date').html(dayNames[newDate.getDay()] + ", " + newDate.getDate() + ' de ' + monthNames[newDate.getMonth()] + ' de ' + newDate.getFullYear());

            setInterval(function () {
                // Create a newDate() object and extract the seconds of the current time on the visitor's
                var seconds = new Date().getSeconds();
                // Add a leading zero to seconds value
                $("#sec").html((seconds < 10 ? "0" : "") + seconds);
            }, 1000);

            setInterval(function () {
                // Create a newDate() object and extract the minutes of the current time on the visitor's
                var minutes = new Date().getMinutes();
                // Add a leading zero to the minutes value
                $("#min").html((minutes < 10 ? "0" : "") + minutes);
            }, 1000);

            setInterval(function () {
                // Create a newDate() object and extract the hours of the current time on the visitor's
                var hours = new Date().getHours();
                // Add a leading zero to the hours value
                $("#hours").html((hours < 10 ? "0" : "") + hours);
            }, 1000);
        },

        directAccess: function () {
            var self = this;
            var userCode = $('#userCode').val();
            var keyboard = $('#keyboard').val();

            if (userCode == ""){
                $('#userCode').val(keyboard);
                // $("#codeZone").addClass("hide");
                $("#passwZone").removeClass("hide");
                $('#lblKeyboard').html("Contraseña código de usuario");
                $('#keyboard').val("");
            }else{

                var passRot =  rot13(keyboard);
                $.ajax({
                    url: '/tracking/direct?userCode=' + userCode + '&codePass=' + passRot,
                    method: 'post',
                    success: function (result) {
                        var objResp = JSON.parse(result);
                        if (objResp.success === true) {
                            $('#completedAccessModal').modal();

                            if (objResp.data.user.status) {
// Create a newDate() object
                                $('#completedAccessModal .modal-body').html("Bienvenido "+ objResp.data.user.name  +", trabaja mucho :)" + "<br/> Hora de registro:" + objResp.data.timeRegister );

                            } else {
                                $('#completedAccessModal .modal-body').html("Hasta luego " + objResp.data.user.name  +", :) " +
                                    " <br/> Hora de registro: " + objResp.data.timeRegister +
                                    " <br/> Horas registradas en el dia de hoy: "+ objResp.data.timeSumary) ;
                            }
                            // $('#userCode').val('');
                            // todo degypsythat, no se como se llama a un metodo del mismo elemento, supondria que seria algo del estilo this.cancelAccess(); pero no
                            $( '.cancelEntrance' ).click ();
                        } else {
                            $('#completedAccessModal').modal();
                            //todo buscar otro texto para este "error", me hizo gracia cuando me salto pero esto no creo que quiera/pueda quedar asi en pro aunque no debiese salir nunca
                            $('#completedAccessModal .modal-body').text("No encuentro el código que has puesto. ¿la habrás liado?");
                            $('#keyboard').val("");
                        }
                    }
                });
            }
        },
        cancelAccess: function () {
            $('#userCode').val("");
            $('#keyboard').val("");
            // $('#userCodePass').val("");
            $("#passwZone").addClass("hide");
            // $("#codeZone").removeClass("hide");
            $('#lblKeyboard').html("Código de usuario");
        }

    });

    // Finally, we kick things off by creating the **App**.
    var App = new AppView();
});
