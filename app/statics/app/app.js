

//Viewport
var AppViewport = Backbone.View.extend({

    // Instead of generating a new element, bind to the existing skeleton of
    // the App already present in the HTML.
    el: $("body"),

    // Our template for the line of statistics at the bottom of the app.
    // statsTemplate: _.template($('#stats-template').html()),

    // Delegated events for creating new items, and clearing completed ones.
    events: {
        "click #userStatusChange": "userStatusChange"
    },


    user: null,
    // At initialization we bind to the relevant events on the `Todos`
    // collection, when items are added or changed. Kick things off by
    // loading any preexisting todos that might be saved in *localStorage*.
    initialize: function (callback) {
        var self = this;
        $.ajax({
            url: "/session",
            success: function (result) {
                var resObj = JSON.parse(result);

                //Ocultar menu administracion
                self.user = resObj.data[0];
                $('#configMainMenuItem').show();
                $('#alertMenuItem').show();
                if (self.user.profile === "User") {
                    $('#configMainMenuItem').hide();
                    $('#alertMenuItem').hide();

                }
                $('#userIdentifierText').html(self.user.username);
                $('#userFullnameContainer').html(self.user.username);
                $('#userEmailContainer').html(self.user.email);
                $('#userCodeContainer').html(self.user.code);

                if (self.user.status) {
                    $('#userStatusChange').text('Check OUT');
                    $('#userStatusChange').removeClass('btn-success').addClass('btn-info');
                } else {
                    $('#userStatusChange').text('Check IN');
                    $('#userStatusChange').removeClass('btn-info').addClass('btn-success');
                }
                callback();
            }
        });


        $(document).ajaxError(function () {
            console.log('error');
            $('.applogo').popover({
                title: 'Error',
                content: 'Se ha producido un error',
                placement: 'bottom'
            });
        });

    },

    // Re-rendering the App just means refreshing the statistics -- the rest
    // of the app doesn't change.
    render: function () {

    },

    userStatusChange: function (e) {
        var self = this;
        var url = '/tracking/:userId/in';
        if (self.user.status) {
            url = '/tracking/:userId/out';
        }
        url = url.replace(':userId', self.user.id);
        $.ajax({
            url: url,
            method: 'post',
            success: function (result) {
                var resObj = JSON.parse(result);
                self.user = resObj.data;
                if (self.user.status) {
                    $('#userStatusChange').text('Check OUT');
                    $('#userStatusChange').removeClass('btn-success').addClass('btn-info');
                } else {
                    $('#userStatusChange').text('Check IN');
                    $('#userStatusChange').removeClass('btn-info').addClass('btn-success');
                }
                window.location.reload();
            }
        });
    },
    require_template: function (templateName) {
        var template = $('#template_' + templateName);
        if (template.length === 0) {
            var tmpl_dir = './templates';
            var tmpl_url = tmpl_dir + '/' + templateName + '.tmpl';
            var tmpl_string = '';

            $.ajax({
                url: tmpl_url,
                method: 'GET',
                async: false,
                contentType: 'text',
                success: function (data) {
                    tmpl_string = data;
                }
            });

            $('head').append('<script id="template_' +
                templateName + '" type="text/template">' + tmpl_string + '<\/script>');
        }
    }

});


// Finally, we kick things off by creating the **AppViewport**.
APP.viewport = new AppViewport(function () {

    APP.router = new APP.AbraxasRouter();
});



