
/**
 * Encripta un string utilizando el algoritmo
 * @returns {string}
 */
function rot13(str) {
    return str.replace(/[a-zA-Z]/g, function (c) {
        return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
    });
};
/**
 * Redefinicion de la funcion console en caso de no existir(fix ie)
 */
if (typeof console === "undefined") {
    console = {}; //define it if it doesn't exist already
}
if (typeof console.log === "undefined") {
    console.log = function () {
    };
}
if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    };
}

/**
 * Completa con tantos ceros como se especifique el string pasado
 * @param a - numero de ceros
 * @param b - string a completar
 * @returns {string}
 */
function pad(a, b) {
    return (1e15 + a + "").slice(-b);
}

/**
 * Pone mayuscula la primera letra de un String
 * @param string
 * @returns {string}
 */
function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * Calcula la edad en funcion de una fecha.
 * @param date
 * @returns {number}
 */
function calculateAge(date) {
    var birthday;
    if (date instanceof Date) {
        birthday = date;
    } else {
        if (date.indexOf("-") !== -1) {
            birthday = +new Date(date.substr(0, 4), date.substr(5, 2) - 1, date.substr(8, 2));
        } else {
            birthday = +new Date(date.substr(0, 4), date.substr(4, 2) - 1, date.substr(6, 2));
        }
    }

    return ~~((new Date().getTime() - birthday) / (31557600000));
}

/**
 * Comprueba si la variable pasada es una funcion
 * @param x
 * @returns {boolean}
 */
function isFunction(x) {
    return Object.prototype.toString.call(x) == '[object Function]';
}


/**
 * Funcion indexof para utilizar en los array. Fix para ie ya que la funcion array.indexOf no funciona correctamente.
 * @param array
 * @param obj
 * @param start
 * @returns {*}
 */
function indexOf(array, obj, start) {
    for (var i = (start || 0), j = array.length; i < j; i++) {
        if (array[i] === obj) {
            return i;
        }
    }
    return -1;
}

/**
 * Comprueba si un objeto esta vacio.
 * @param obj
 * @returns {boolean}
 */
function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}



/**
 * Elimina de una String varios caracteres que pueden causar problemas involuntarios si usamos dicha String para
 * la evaluacion de una expresion regular
 * @param str
 * @returns {void|XML|string|*}
 */
function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "");
}

/**
 * Comprueba la existencia de un elemento en un array mediante una clave.
 * @param array
 * @param key
 * @param elementCheck
 * @returns {boolean}
 */
function existsInArray(array, key, elementCheck) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][key] === elementCheck) {
            return true;
        }
    }
    return false;
}


function getWordsBetweenCurlies(str) {
    var results = [], re = /{([^}]+)}/g, text;

    while (text = re.exec(str)) {
        results.push(text[1]);
    }
    return results;
}


function jsonToQueryString(json) {
    return '' +
        Object.keys(json).map(function (key) {
            return encodeURIComponent(key) + '=' +
                encodeURIComponent(json[key]);
        }).join('&');
}