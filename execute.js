//module dependencies.
require('app-module-path').addPath(__dirname);
require("app/lib/Class.js");

//Carga de utilidades
global.utils = require('app/server/common/Utils.js');
//Carga de configuraciones
var Settings = require('app/server/common/Settings.js');
global.settings = new Settings().load();

//Inicia cargador de querys
var QueryHandler = require('app/server/common/QueryHandler.js');
global.querys = new QueryHandler();

//Conexion con base de datos
if (global.settings.getConfigValue("mysql").database !== "") {
    var mysql = require("mysql");
    var con = mysql.createPool({
        host: global.settings.getConfigValue("mysql").host,
        user: global.settings.getConfigValue("mysql").user,
        password: global.settings.getConfigValue("mysql").password,
        database: global.settings.getConfigValue("mysql").database
    });
    //Test de conexion de mysql
    con.getConnection(function (err) {
        if (err) {
            console.error('Error conectando con la base de datos');
            process.exit(1);
        }
    });
    global.con = con;
}
global.constant = {};
global.constant.SuperAdmin = 1;
global.constant.Admin = 2;
global.constant.User = 3;

var EventHandler = require('app/server/common/EventHandler.js');
global.eventHandler = new EventHandler();
require('app/server/events/GroupRuleEventListener.js').init();

//Cargar despues del queryhandler y de la conexión
// global.settings.loadFromBD();

//var encoded = global.utils.encrypt('hola');
//console.log(encoded);
//console.log(global.utils.decrypt(encoded));

//Carga logger para redirigir a un archivo
var Logger = require("app/server/common/Logger.js");
var log = new Logger();

//Inicio del cluster server
var ClusterServer = require('app/server/ClusterServer.js');

var clServ = new ClusterServer(process, global.settings.getConfigValue("server").port);

//Capturar errores perdidos
process.on('uncaughtException', function (err) {
    // handle the error safely
    console.error("Error: %s", err.stack || err.message);
});